(function() {
	$(document).ready(function() {		
		// viewport is greater than, or equal to, 768 pixels wide
		if (window.matchMedia("(min-width: 768px)").matches) {			
			$(".story-footer").find( "a" ).removeClass("btn-sm");
			$(".story-footer").find( "a" ).addClass("btn-md");
		}
	});
})()