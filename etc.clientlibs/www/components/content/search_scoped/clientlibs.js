(function ($) {

	COC.ScopedSearch = {
		removeFilterClickHandler: function() {
			$(document).on("click", "a.selected-dynamic-filter", function(e) {

				var clickedFilter = $(this)[0].id;
				if (clickedFilter.length === 0) {
					return;
				}
				clickedFilter = clickedFilter.split("-")[1];
				clickedFilter = clickedFilter.replace(/\s+/g, '+');

				let finalUrl = '';
				var params = window.location.search.split('&');
				for (let i = 0; i < params.length; i++) {
					if (params[i].length === 0) {
						continue;
					}

					if (params[i].startsWith('filter=' + clickedFilter)) {
						continue;
					}

					// do not remember page number when removing selected filters
					if (params[i].startsWith('page=')) {
						continue;
					}

					finalUrl += params[i] + '&';
				}

				finalUrl = finalUrl.substring(0, finalUrl.length - 1);
				window.location = finalUrl;
				e.preventDefault();
			});
		}
	}

	$(document).ready(function() {
		COC.ScopedSearch.removeFilterClickHandler();
	});
})(jQuery);