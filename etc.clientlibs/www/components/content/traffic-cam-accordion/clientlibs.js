(function ($) {

	COC.TrafficCamerasAccordion = 
	{
		serveAccordion: function ()
		{
			var defaultExpanded = $( "#Traffic_Cam_Accordion_Object" ).hasClass("default-expanded");
			var WindowWidth = $( window ).width();
			
			if (!defaultExpanded || WindowWidth <= "768" )
			{
				$( "#Traffic_Cam_Accordion_Object" ).removeClass( "default-expanded" );
				$("button.cui.btn-sm.secondary-text.show-hide-all-button.mb-xxs").attr("aria-pressed", false).text("Expand all");
				$( "#Traffic_Cam_Accordion_Object" ).find('.title-bar').each(function (index) {
					$(this).next().css("display", "none" );
					$(this).next().attr("aria-hidden", true);
					$(this).children().attr("aria-expanded", false);
				});

				COC.TrafficCamerasAccordion.InteractWithTabs(camObject);

				$("button.cui.btn-sm.secondary-text.show-hide-all-button.mb-xxs").one('click', function(e) 
				{
					COC.TrafficCamerasAccordion.GenerateFullBody();

					$("button.cui.btn-sm.secondary-text.show-hide-all-button.mb-xxs").attr("aria-pressed", true).text("Collapse all");
					$( "#Traffic_Cam_Accordion_Object" ).find('.title-bar').each(function (index) {
						$(this).next().css("display", "block" );
						$(this).next().attr("aria-hidden", false);
						$(this).children().attr("aria-expanded", true);
					});
				});
			}
			else
			{
				COC.TrafficCamerasAccordion.GenerateFullBody();
			}
		},

		getCameraObject: function() {
			var paramList = "";
			$.ajax({
				type: "GET",
				async: false,
				url: 'https://services1.arcgis.com/AVP60cs0Q9PEA8rH/ArcGIS/rest/services/TrafficCameras_Public/FeatureServer/0/query?where=1%3D1&outFields=*&returnGeometry=false&outSR=4326&f=pjson',
				data: "{}",
				contentType: "text/plain",
				dataType: "text",
				success: function (msg) {
					var data = $.parseJSON(msg);
					paramList = COC.TrafficCamerasAccordion.TransformData(data.features);

				},
				error: function (jqXHR, textStatus, errorThrown) {
					console.log("get session failed " + errorThrown);
				}
			});
			return paramList;
		},

		InteractWithTabs: function(camObject) {

			var AccordionHeaderObject = [];

			var AccordionHeaderObject = '{"AccordionHeader":[]}';
			var obj = JSON.parse(AccordionHeaderObject);

			$( ".title-bar" ).one('click', function(e) {
				var myId = $(this).attr("id");
				e.preventDefault();
				// DisplayCamerasPerSection
				if (myId !="trafficcamaccorionfaq")
				{
					console.log( myId.replace('_', ''));

					COC.TrafficCamerasAccordion.DisplayCamerasPerSection(camObject,myId.replace('_', ' '));
					obj['AccordionHeader'].push( myId );
					AccordionHeaderObject = JSON.stringify(obj);
				}

				// Allow to expand:
				$(this).click();
			});
		},

		GenerateFullBody: function() 
		{
			jQuery.ajax({
				url: 'https://services1.arcgis.com/AVP60cs0Q9PEA8rH/ArcGIS/rest/services/TrafficCameras_Public/FeatureServer/0/query?where=1%3D1&outFields=*&returnGeometry=true&outSR=4326&f=pjson',
				type: "GET",
				contentType: "text/plain",
				dataType: "text",
				success: function(data) {
					var text = data;
					var obj = JSON.parse(text);
					var dataSet = COC.TrafficCamerasAccordion.TransformData(obj.features);
					COC.TrafficCamerasAccordion.RenderTrafficAccordion(dataSet);

				},
				async: true
			});
		},

		TransformData: function(CityTrafficCamerasList)
		{
			var dataSet = [];
			var i;
			var globalID;
			var ID;
			var ListType;
			var Description;
			var Quadrant;
			var Title;
			var URL;
			var IsPublic;
			var GlobalID;
			var record;
			for (i = 0; i < CityTrafficCamerasList.length; i++) { //[""0""].attributes.Description
				globalID = CityTrafficCamerasList[i].attributes.GlobalID || "";
				ID = CityTrafficCamerasList[i].attributes.OBJECTID || "";
				ListType = CityTrafficCamerasList[i].attributes.ListType || "";
				Description = CityTrafficCamerasList[i].attributes.Description || "";
				Quadrant = CityTrafficCamerasList[i].attributes.Quadrant || "";
				Title = CityTrafficCamerasList[i].attributes.Title || "";
				URL = CityTrafficCamerasList[i].attributes.URL || "";
				IsPublic = CityTrafficCamerasList[i].attributes.IsPublic || "";

				record = {

					"Description": Description,
					"Quadrant": Quadrant,
					"Title": Title,
					"URL": URL,
					"GlobalID":globalID,
					"IsPublic": IsPublic,
				};

				dataSet.push(record);
			}
			return dataSet;
		},
		RenderTrafficAccordion : function(dataSet)
		{
			var j = "";
			var i = "";
			var AccordionBody = "";
			var AccordionHeader = "";
			var AccordionHeaderObject = "";
			var count = 0;
			var currentsearchterm = "";
			var headercount = 0;
			var internaldiv = "";
			var imgurl = "";
			var comparewith = "";
			var accordiongroupby = "";
			var currentsearchtermtitle = "";
			var onlinestatus;
			var unqiueid;
			var CityQuadrant = {
				NE: "Northeast",
				NW: "Northwest",
				SE: "Southeast",
				SW: "Southwest"
			};

			var urlRegEx = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-]*)?\??(?:[\-\+=&;%@\.\w]*)#?(?:[\.\!\/\\\w]*))?)/g;
			var CameraPath;

			AccordionHeaderObject = {
				"AccordionHeader": ["16 Avenue", "Bow Trail",
									"Crowchild Trail", "Deerfoot Trail",
									"Glenmore Trail", "Macleod Trail",
									"Memorial Drive", "Stoney Trail", "NE", "NW", "SE", "SW"]
			};
			
			for (j = 0; j < AccordionHeaderObject.AccordionHeader.length; j++) 
			{
				currentsearchterm = AccordionHeaderObject.AccordionHeader[j];
				currentsearchterm = AccordionHeaderObject.AccordionHeader[j];
				
				if (currentsearchterm == "NE" || currentsearchterm == "NW" || currentsearchterm == "SE" || currentsearchterm == "SW") {
					accordiongroupby = "Quadrant";
					currentsearchtermtitle = CityQuadrant[currentsearchterm];
				} else {
					currentsearchtermtitle = AccordionHeaderObject.AccordionHeader[j];
					accordiongroupby = "Title";
				}
				
				unqiueid = currentsearchtermtitle+"_"+j;

				var colcount = 0;
				for (i = 0; i < dataSet.length; i++) {
					GlobalID = dataSet[i].GlobalID;
					AccordionElemntID = GlobalID.replace('-','');

					AccordionBody = dataSet[i].Title;
					imgurl = dataSet[i].URL;
					if (accordiongroupby == "Quadrant") {
						searchby = dataSet[i].Quadrant;
					} else {
						searchby = dataSet[i].Title;
					}

					onlinestatus = dataSet[i].IsPublic;

					if (onlinestatus.toLowerCase() == "yes") {
						CameraPath = imgurl.match(urlRegEx);
					} else {
						CameraPath = "https://www.calgary.ca/Transportation/Roads/Scripts/Traffic/TrafficCameras/images/cameraOffline.jpg";
					}

					if (searchby.indexOf(currentsearchterm) != -1) {
						colcount++;
						if (colcount == 1) {
							internaldiv = "<div class='row'>";
						}
						var cleanstreetname = AccordionBody.replace("/", "and");

						//internaldiv += "<div class='col-sm-6'><span><button role='button' class='cui btn-tooltip btn-md utility-btn'  aria-label='Click here to View Enlarged Image Of Traffic Camera At: " + cleanstreetname + " ' data-title='Click to View Enlarged Image Of Traffic Camera At: " + cleanstreetname + " '  data-placement='top' ><img  onclick='viewcam(this.src,this.alt);'  style='cursor: pointer;'  src='" + CameraPath + "'  aria-label='Click here to View Enlarge Image Of Traffic Camera At: " + cleanstreetname + " ' tabindex='"+i+"'  alt= 'Traffic Camera at " + cleanstreetname + "' ></button></span><br><span class='caption'> <p style='text-align:center;font-weight: bold; '>" +  AccordionBody + "</p></span> </div>";
						internaldiv += "<div class='col-12 col-md-4'><a style='cursor: pointer;' data-toggle='modal'  data-target='#trafficModal' data-dynamicimg='"+ CameraPath+"' data-dynamictitle='"+cleanstreetname+"'><img src='"+CameraPath+"' data-toggle='tooltip' title='Click here to view enlarge image of traffic camera at:"+cleanstreetname+"' alt= 'Traffic Camera at " + cleanstreetname + "'></a><p style='text-align:center;margin-top: 1.25rem;margin-buttom: 1.25rem!important;'>"+cleanstreetname+"</p></span></div>";
						if (colcount %3 == 0) {
							internaldiv += "</div>";
							internaldiv += " <div class='row'>";

						}
					}

					var divname = currentsearchterm.replace(' ','');

					$( "div."+divname).html(internaldiv); 
				}
			}

		},

		DisplayCamerasPerSection : function(dataSet,Section)
		{
			var j = "";
			var i = "";
			var AccordionBody = "";
			var AccordionHeader = "";
			var AccordionHeaderObject = "";
			var count = 0;
			var currentsearchterm = "";
			var headercount = 0;
			var internaldiv = "";
			var imgurl = "";
			var comparewith = "";
			var accordiongroupby = "";
			var currentsearchtermtitle = "";
			var onlinestatus;
			var unqiueid;
			
			var CityQuadrant = {
				NE: "Northeast",
				NW: "Northwest",
				SE: "Southeast",
				SW: "Southwest"
			};

			var urlRegEx = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-]*)?\??(?:[\-\+=&;%@\.\w]*)#?(?:[\.\!\/\\\w]*))?)/g;
			var CameraPath;

			currentsearchterm = Section;

			if (currentsearchterm == "NE" || currentsearchterm == "NW" || currentsearchterm == "SE" || currentsearchterm == "SW") {
				accordiongroupby = "Quadrant";
				currentsearchtermtitle = CityQuadrant[currentsearchterm];
			} else {
				currentsearchtermtitle = Section;
				accordiongroupby = "Title";
			}
			
			unqiueid = currentsearchtermtitle+"_"+j;

			var colcount = 0;
			for (i = 0; i < dataSet.length; i++) {
				GlobalID = dataSet[i].GlobalID;
				AccordionElemntID = GlobalID.replace('-','');

				AccordionBody = dataSet[i].Title;
				imgurl = dataSet[i].URL;
				if (accordiongroupby == "Quadrant") {
					searchby = dataSet[i].Quadrant;
				} else {
					searchby = dataSet[i].Title;
				}

				onlinestatus = dataSet[i].IsPublic;

				if (onlinestatus.toLowerCase() == "yes") {
					CameraPath = imgurl.match(urlRegEx);
				} else {
					CameraPath = "https://www.calgary.ca/Transportation/Roads/Scripts/Traffic/TrafficCameras/images/cameraOffline.jpg";
				}

				if (searchby.indexOf(currentsearchterm) != -1) {
					colcount++;
					if (colcount == 1) {
						internaldiv = "<div class='row'>";
					}
					var cleanstreetname = AccordionBody.replace("/", "and");

					internaldiv += "<div class='col-12 col-md-4'><a style='cursor: pointer;' data-toggle='modal'  data-target='#trafficModal' data-dynamicimg='"+ CameraPath+"' data-dynamictitle='"+cleanstreetname+"'><img src='"+CameraPath+"' data-toggle='tooltip' title='Click here to view enlarge image of traffic camera at:"+cleanstreetname+"' alt= 'Traffic Camera at " + cleanstreetname + "'></a><p style='text-align:center;margin-top: 1.25rem;margin-buttom: 1.25rem!important;'>"+cleanstreetname+"</p></span></div>";
					if (colcount %3 == 0) {
						internaldiv += "</div>";
						internaldiv += " <div class='row'>";

					}
				}

				var divname = currentsearchterm.replace(' ','');

				$( "div."+divname).html(internaldiv); 
			}
		}
	}

})(jQuery);

(function ($) {
	
	COC.TrafficCameraModal = 
	{
		LoadDynamicTrafficImage: function() {

			$('#trafficModal').on('hidden.bs.modal', function (event) {
				var modal = $(this)
				modal.find('#dynamic').attr('src','');
				modal.find('#dynamic').attr('alt', '');
				modal.find('.modal-title').text('')

			});

			$('#trafficModal').on('show.bs.modal', function (event) {
				var button = $(event.relatedTarget) // Button that triggered the modal
				var dynamicimg = button.data('dynamicimg') // Extract info from data-* attributes
				var dynamictitle = button.data('dynamictitle') // Extract info from data-* attributes
				var modal = $(this)
				modal.find('#dynamic').attr('src',dynamicimg);
				modal.find('#dynamic').attr('alt', 'Traffic camera at ' + dynamictitle);
				modal.find('.modal-title').text('Traffic camera at ' + dynamictitle)
			});
		}
	}
})(jQuery);
(function ($) {

COC.TrafficCameraModal = 
    {

LoadDynamicTrafficImage: function() {
	
	 $('#trafficModal').on('hidden.bs.modal', function (event) {
		 		  var modal = $(this)
		  modal.find('#dynamic').attr('src','');
		  modal.find('#dynamic').attr('alt', '');
		  modal.find('.modal-title').text('')

		});
	 
 $('#trafficModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var dynamicimg = button.data('dynamicimg') // Extract info from data-* attributes
  var dynamictitle = button.data('dynamictitle') // Extract info from data-* attributes
  var modal = $(this)
  modal.find('#dynamic').attr('src',dynamicimg);
  modal.find('#dynamic').attr('alt', 'Traffic camera at ' + dynamictitle);
  modal.find('.modal-title').text('Traffic camera at ' + dynamictitle)

});

}
   }

//$( document ).ready(function() {

//});
})(jQuery);
