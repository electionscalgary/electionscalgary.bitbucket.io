var COC = COC || {};
COC.Components = COC.Components || {};
COC.Components.AssessmentChatbot = COC.Components.AssessmentChatbot || {};
COC.Components.AssessmentChatbot = function (ID) {
    var _this = this;

    var componentID = ID;

    function isBrowserIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE");
        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
            return true;
        }
        return false;
    }

    function createEvents() {
        $("#AssessmentChatbotButton").click(function () {
            if (isBrowserIE()) {
                $("#AssessmentChatbotIframeContainer iframe").remove();
                $("#AssessmentChatbotIframeContainer").css("border", "1px solid #000000");
                $("#AssessmentChatbotIframeContainer").css("background-color", "#ededee");
                $("#AssessmentChatbotIframeContainer").css("height", "225px");
                $("#AssessmentChatbotIframeContainerButton span").css("color", "#000000");
                $("#AssessmentChatbotIEMessage").css("display", "block")
            } else {
                if (COC.Meta.environment == "PROD") {
                    $("#AssessmentChatbotIframeContainer").append("<iframe width='100%' height='100%' src='https://assessmentsearch.calgary.ca/chatbot.aspx'></iframe>");
                } else {
                    $("#AssessmentChatbotIframeContainer").append("<iframe width='100%' height='100%' src='https://asmtocstest.calgary.ca/chatbot.aspx'></iframe>");
                }
            }
            $("#AssessmentChatbotButton").css("display", "none");
            $("#AssessmentChatbotIframeContainer").hide();
            function show_chatbot() {
                $("#AssessmentChatbotIframeContainer").show();
            };
            window.setTimeout(show_chatbot, 1000);
        });
        $("#AssessmentChatbotIframeContainerButton").click(function () {
            $("#AssessmentChatbotButton").css("display", "block");
            $("#AssessmentChatbotIframeContainer").css("display", "none");
            $("#AssessmentChatbotIframeContainer iframe").remove();
        });
    }

    this.Init = function () {
        createEvents();
    }
};