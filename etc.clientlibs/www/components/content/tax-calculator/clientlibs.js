/**
* Revenue Neutral Calculator Functions
* Files: https://spprd-authoring.calgary.ca:47443/PDA/Assessment/Scripts/Forms/AllItems.aspx?RootFolder=%2fPDA%2fAssessment%2fScripts%2frevenue_neutral_calculator&FolderCTID=0x01200034A9BCC6D622F347AB49ECA3843378A5
*
* @author Chirag, Butani , Digital Developer, chirag.butani@calgary.ca
*/

/*
var COC = COC || {};

COC.Components.RevenueNeutralCalculator = (function calculator(element) {
	
  // the values sent from assessment.
  var taxRates =
    {
      dates: {
        current_year: 2020,
        previous_year: 2019,
        other_year: 2018
      },
      current_year:{
        res: {
          municipal: {
            current_municipal_tax_RN: 0.0043844,
            //current_municipal_tax_rate: 0.0042108,
            current_municipal_council_budget_decision: 0.0000658,
            current_municipal_rebate: -0.0000658,
            current_municipal_revenue_shift: 0.0002964,
            flow_through: 0.0000329
          },
          provincial: {
            current_provincial_tax_RN: 0.0025488,
            current_provincial_budget_decision: 0.0001716,
            current_provincial_catch_up: 0.0001152
          }
        },
        non_res: {
          municipal: {
            current_municipal_tax_RN: 0.0176147,
            //current_municipal_tax_rate: 0.0176057,
            current_municipal_council_budget_decision: 0.0002084,
            current_municipal_rebate: -0.0002084,
            current_municipal_revenue_shift: -0.0020995,
            flow_through: 0.0001042
          },
          provincial: {
            current_provincial_tax_RN: 0.0042207,
            current_provincial_budget_decision: -0.0003696,
            current_provincial_catch_up: -0.0001459
          }
        },
        farm: {
          municipal: {
            current_municipal_tax_RN: 0.0189394,
            //current_municipal_tax_rate: 0.0189394,
            current_municipal_council_budget_decision: 0.0002841,
            current_municipal_rebate: -0.0002841,
            current_municipal_revenue_shift: 0.0012803,
            flow_through: 0.0001420
          },
          provincial: {
            current_provincial_tax_RN: 0.0025488,
            current_provincial_budget_decision: 0.0001716,
            current_provincial_catch_up: 0.0001152
          }
        },
        split: {municipal: {}, provincial:{}}
      },
      previous_year: {
        res: {
          business_tax_consolodation: 0,
          municipal: {
            previous_municipal_tax_rate: 0.0042108
          },
          provincial: {
            previous_provincial_tax_rate: 0.0024432
          },
          previous_municipal_total_tax: 0,
          previous_provincial_total_tax: 0,
          previous_total_tax: 0
        },
        non_res: {
          get business_tax_consolodation() {
            return parseFloat(digits(elements.prev_assessment_input.value)) * 0.0007411;
          },
          municipal: {
            previous_municipal_tax_rate: 0.0177750
          },
          provincial: {
            previous_provincial_tax_rate: 0.0042467
          },
          previous_municipal_total_tax: 0,
          previous_provincial_total_tax: 0,
          previous_total_tax: 0
        },
        farm: {
          municipal: {
            previous_municipal_tax_rate: 0.0189394,
            business_tax_consolodation: 0
          },
          provincial: {
            previous_provincial_tax_rate: 0.0024432
          },
          previous_municipal_total_tax: 0,
          previous_provincial_total_tax: 0,
          previous_total_tax: 0
        },
        split: {municipal: {}, provincial:{}}
      },
      other_year: {
        res: {
          municipal: {
            other_municipal_tax_rate: 0.0039014
          }
        },
        non_res: {
          municipal: {
            other_municipal_tax_rate: 0.0153234
          }
        },
        farm: {
          municipal: {
            other_municipal_tax_rate: 0.0177552
          },
        },
        split: {municipal: {}, provincial:{}}
      }
    },
    currentValue = 0,
    previousValue = 0,
    otherValue = 0,
    done = false,
    taxType = 'res',
    splitClass = false,
    nonRes = false,
    splitClassRebate = {current:0, past:0};
    
    
    function setElements() {
		return elements = {
				container: document.getElementById('calculator'),
		        assessment_input: document.getElementById('CurrentYearAssessment'),
		        prev_assessment_input: document.getElementById('PreviousYearAssessment'),
		        assessment_input_2018: document.getElementById('Assessment-2018'),
		        submit: document.getElementById('submit_calculator'),
		        showMuni: document.getElementById('showAllMuni'),
		        showProv: document.getElementById('showAllProv'),
		        clear: document.getElementById('clear_calculator'),
		        type: document.getElementById('tax_type'),
		        outputs: document.querySelectorAll('.tax'),
		        total: document.getElementById('total_taxes'),
		        table: document.getElementById('tax_table'),
		        tr: document.getElementsByTagName('tr'),
		        percents: document.querySelector('.percentages'),
		        get percentInputs() {
		          return this.percents.querySelectorAll('input');
		        },
		        get currentPercents() {
		          return this.percents.querySelectorAll('.current-year-percents input');
		        },
		        get pastPercents() {
		          return this.percents.querySelectorAll('.previous-year-percents input');
		        },
		        get otherPercents() {
		          return this.percents.querySelectorAll('.other-year-percents input');
		        }
		}
	}

  *//**
  * sets up data, listeners, etc...
  * @return {void};
  *//*
  function init() {
	  elements = setElements();
    // update the dates on the page
    setYear(taxRates.dates.current_year, '.this-year');
    setYear(taxRates.dates.previous_year, '.last-year');
    // listen for the changing the 'assessment class/business' select box.
    elements.type.addEventListener('change', changeTypeHandler, false);
    // button click listeners
    elements.submit.addEventListener('click', submitHandler, false);
    elements.clear.addEventListener('click', clearHandler, false);
    elements.showMuni.addEventListener('click', showHide, false);
    elements.showProv.addEventListener('click', showHide, false);
    // validate input
    elements.assessment_input.addEventListener('keydown', assessmentInputHandler, false);
    elements.prev_assessment_input.addEventListener('keydown', assessmentInputHandler, false);
    elements.assessment_input_2018.addEventListener('keydown', assessmentInputHandler, false);
    elements.assessment_input.addEventListener('invalid', function(event){
      event.preventDefault();
    }, false);

    var percents = elements.percentInputs;
    for (var i = 0; i < percents.length; i++ ) {
      percents[i].addEventListener('keydown', percentInputHandler, false);
      percents[i].addEventListener('focus', percentClickHandler, false);
      percents[i].addEventListener('invalid', function(event){
        event.preventDefault();
      }, false);
    }

    // check for query string variables from assessment search site.
    checkQueryString();
  }

  *//**
  * handles search button clicks
  * @param {MouseEvent} event - a click on the search button
  * @return {false} - returns false if there are no values in the form inputs.
  *//*
  function submitHandler(event) {
    currentValue = digits(elements.assessment_input.value) || 0;

    if(nonRes || splitClass){
      previousValue = digits(elements.prev_assessment_input.value) || 0;
      otherValue = digits(elements.assessment_input_2018.value) || 0;
    }else{
      previousValue = 0;
      otherValue = 0;
    }
    var percentData = null;

    elements.assessment_input.value = commafy(currentValue);

    if (splitClass) {
      percentData = {};
      percentData.current = checkPercents(elements.currentPercents);
      if(previousValue){
        percentData.previous = checkPercents(elements.pastPercents);
      }
      if(otherValue){
        percentData.other = checkPercents(elements.otherPercents);
      }
    }

    update(currentValue, percentData, 'current');

    if (previousValue > 0) {
      elements.prev_assessment_input.value = commafy(previousValue);
      update(previousValue, percentData, 'previous')
    }

    if (otherValue > 0) {
      elements.assessment_input_2018.value = commafy(otherValue);
      update(otherValue, percentData, 'other')
    }

    done = true;

    event.stopPropagation();
    event.preventDefault();
  }

  function showHide(event){

    var button = event.target;
    var hidden = button.hasAttribute("data-hidden");
    if(hidden){
      elements.table.classList.add('show-' + button.name)
      button.removeAttribute('data-hidden');
      button.innerText = 'Hide all';
    }else{
      elements.table.classList.remove('show-' + button.name)
      button.setAttribute('data-hidden', true);
      button.innerText = 'Show all';
    }
    // rows.forEach(function(row){
    //   setARIAs(row);
    // })
    event.preventDefault();
  }

  function update(value, percents, year){

    currentValue = value;
    taxRates[year + '_year'].split = {municipal: {}, provincial:{}}
    var rates = taxRates[year + '_year'][taxType];

    console.log(taxRates, taxType, rates)

    if (nonRes || splitClass) {
      nonResRebate(rates, percents);
    }

    if(percents && percents.current){
      rates = getRatesByPercentage(value, percents, year)
    }else{
      rates = getRate(value, rates, year)
    }
    //console.log('UPDATE', percents, year, taxType, rates)
    setRates(rates);

    return false;
  }


  function getRatesByPercentage(value, percentages, year){
    var totals = 0;
    var total_tax = 0;
    var category = ['municipal', 'provincial'];
    var rates = taxRates[year + '_year'];
    var percents = percentages[year];
    var muniRates = rates.split.municipal;
    var provRates = rates.split.provincial;
    var values, val, rate, revNeutral, decision, rebate, flowThrough, btc, percent, shift, catchup;
    value = parseFloat(value);

    for(type in rates){
      if (type === 'split') { continue; }
      percent = percents[type];

      if(year === 'current'){
        category.forEach(function(cat, i){
          if(!rates[type][cat]){
            return;
          }
          // rate = rates[type][cat]['current_' + cat + '_tax_rate'];
          // rates.split[cat]['current_' + cat + '_tax_rate'] = rates.split[cat]['current_' + cat + '_tax_rate'] || 0;
          // rates.split[cat]['current_' + cat + '_tax_rate'] += rate * percent;

          revNeutral = rates[type][cat]['current_' + cat + '_tax_RN'];
          rates.split[cat]['current_' + cat + '_tax_RN'] = rates.split[cat]['current_' + cat + '_tax_RN'] || 0;
          rates.split[cat]['current_' + cat + '_tax_RN'] += revNeutral * percent;
                    console.log('revneutra', year, type, cat, percent, revNeutral, revNeutral * percent, percents, percentages)
        })

          catchup = rates[type].provincial['current_provincial_catch_up'];
          provRates['current_provincial_catch_up'] = provRates['current_provincial_catch_up'] || 0;
          provRates['current_provincial_catch_up'] += catchup * percent;

          p_decision = rates[type].provincial['current_provincial_budget_decision'];
          provRates['current_provincial_budget_decision'] = provRates['current_provincial_budget_decision'] || 0;
          provRates['current_provincial_budget_decision'] += p_decision * percent;

          m_decision = rates[type].municipal['current_municipal_council_budget_decision'];
          muniRates['current_municipal_council_budget_decision'] = muniRates['current_municipal_council_budget_decision'] || 0;
          muniRates['current_municipal_council_budget_decision'] += m_decision * percent;

          rebate = rates[type].municipal['current_municipal_rebate'];
          muniRates['current_municipal_rebate'] = muniRates['current_municipal_rebate'] || 0;
          muniRates['current_municipal_rebate'] += rebate * percent;

          flowThrough = rates[type].municipal['flow_through'];
          muniRates['flow_through'] = muniRates['flow_through'] || 0;
          muniRates['flow_through'] += flowThrough * percent;

          shift = rates[type].municipal['current_municipal_revenue_shift'];
          muniRates['current_municipal_revenue_shift'] = muniRates['current_municipal_revenue_shift'] || 0;
          muniRates['current_municipal_revenue_shift'] += shift * percent;

        }

      if(year === 'previous' && type === 'non_res'){
        //business_tax_consolodation
        btc = rates[type].business_tax_consolodation;
        rates.split['business_tax_consolodation'] = btc * percents[type];
      }

    //   // PTP
    //   if(type === 'non_res'){
    //     if(rates.split.ptp_credit)
    // //    nonResRebate()
    //     //console.log('PTP RATES', rates.split)
    //   }

    }
console.log('rates', rates)
    rates = getRate(value, rates.split, year)

    return rates;

  }

  function getRate(value, rates, year){
    var totals = 0;
    var total_tax = 0;
    var values, val, rate;
    var category = ['municipal', 'provincial'];
    value = parseFloat(value);

    //console.log(rates)

    category.forEach(function(cat){

      values = rates[cat];

      totals = 0;
      for(rate in values){

        val = values[rate] * value;
        // if(rate.indexOf('RN') > -1){
        //   rates[rate] = val;
        //   continue;
        // }
        if((nonRes || splitClass) && rate === 'ptp_credit'){
          val = values[rate];
        }
        rates[rate] = val;
        totals += parseFloat(toFixed(val, 2));
        console.log('values', rate, 'val', val, 'total', totals)
        //console.log(rate, val, values[rate], totals)
      }

      rates[year + '_' + cat + '_total_tax'] = totals;
      total_tax += totals;
      console.log('totals', year + '_' + cat + '_total_tax', totals, total_tax)

      //console.log(cat, 'totals', totals, total_tax)
    })
    rates[year + '_' + 'total_tax'] = total_tax;
console.log(rates)
    return rates;
  }

  function toFixed( num, precision ) {
    return (+(Math.round(+(num + 'e' + precision)) + 'e' + -precision)).toFixed(precision);
  }

  function setRates(taxes){
    var el, rate;
    for (rate in taxes) {
      //console.log('setRates', rate, taxes[rate])
      if (taxes.hasOwnProperty(rate)) {
        // add the rates to the page -> using ID's that are the same as the object keys.
        el = document.getElementById(rate);
        if (!el) {
          continue;
        }
        // add the rates to a data attribute.
        el.innerHTML = formatMoney(taxes[rate]);
      }
    }
  }

  *//**
  * handles clear button clicks
  * @param {MouseEvent} event - a click on the clear button
  * @return {void};
  *//*
  function clearHandler(event) {
    var els = elements.outputs;
    var len = elements.outputs.length;
    var i, percents;
    elements.assessment_input.value = '';
    if (nonRes || splitClass) {
      elements.prev_assessment_input.value = '';
      elements.assessment_input_2018.value = '';
    }
    if (splitClass) {
      percents = elements.percentInputs;
      for (i = 0; i < percents.length; i++ ) {
        percents[i].value = '';
      }
    }

    for(i=0; i<len; i++){
        els[i].innerHTML = '';
    }

    event.stopPropagation();
    event.preventDefault();
  }

  *//**
  * handles the assessment value input
  * isNumeric enforces that only numbers are allowed.
  * @param {KeyboardEvent} event - a click on the search button
  * @return {false};
  *//*
  function assessmentInputHandler(event) {
    // showButton('none');
    if (event.key === 'Enter' || event.keyCode === 13) {
      submitHandler(event);
      return false;
    }
    isNumeric(event);
    return false;
  }

  *//**
  * handles the percent input
  * @param {KeyboardEvent} event - a keydown on the percent inputs
  * @return {false};
  *//*
  function percentInputHandler(event) {
    assessmentInputHandler(event);
    isPercent(event);
    return false;
  }

  *//**
  * handles percent input click
  * @param {KeyboardEvent} event - a click on the percent inputs
  * @return {false};
  *//*
  function percentClickHandler(event) {
    event.target.value = '';
    return false;
  }


  *//**
  * handles the select/option box
  * @param {ChangeEvent} event - the select/option value changing
  * @return {void};
  *//*
  function changeTypeHandler(event) {
    var type = event.target.value;
    changeType(type);
  }

  *//**
  * change the calculator values based on the select/optoin box
  * @param {string} type - the value of the select/option box
  * @return {false};
  *//*
  function changeType(type) {
    var i;
    taxType = type;
    elements.container.className = type;
    splitClass = type === 'split' ? true : false;
    nonRes = type === 'non_res' ? true : false;

    if(!splitClass && !nonRes){
      previousValue = 0;
      otherValue =0;
    }

    // add aria-hidden to hidden TR tags.
    for (i = 0; i < elements.tr.length; i++) {
      setARIAs(elements.tr[i]);
    }
    // // update the table values

    if (done) {
      elements.submit.click();
    }
  }

  *//**
  * auto populate form fields in assessmentsearch.calgary.ca based on the query string parameters
  * @return {false};
  *//*
  function checkQueryString() {
    //
    var hashParams = window.location.hash.substr(1).split('&'), // substr(1) to remove the `#`
      value, i, param;

    if (hashParams.length < 2) {
      changeType('res');
      return false;
    }
    for (i = 0; i < hashParams.length; i++) {
      param = hashParams[i].split('=');
      if (param[0] === 'AssessmentClass') {
        value = document.querySelector('.' + param[1]).value;
        elements.type.value = value;
        changeType(value);
        continue;
      } else if (param[0] === 'CurrentYearAssessment') {
        value = decodeURIComponent(param[1]);
        elements.assessment_input.value = commafy(value);
        continue;
      } else if (param[0] === 'PreviousYearAssessment') {
        value = decodeURIComponent(param[1]);
        elements.prev_assessment_input.value = commafy(value);
        continue;
      } else if (param[0] === 'percents') {
        var per = decodeURIComponent(param[1]).split(',');
        var inputs = elements.percentInputs;
        for (var i=0; i<inputs.length; i++){
          inputs[i].value = per[i];
        };
        continue;
      } else if (param[0] === 'OtherYearAssessment'){
        value = decodeURIComponent(param[1]);
        elements.assessment_input_2018.value = commafy(value);
        continue;
      }
    }
    elements.submit.click();
    return false;
  }

  *//**
  * to calculate '2018 Economic Rebate for Non-Residential' - based on unexpected council decision
  * @return {false} - if there's no amount in the previous year assessment input
  *//*
  function nonResRebate(rates, percents) {

    var current = taxRates.current_year.non_res.municipal;
    var previous = taxRates.previous_year.non_res;
    var split = '';

    var currentRate = current.current_municipal_council_budget_decision;
    var currentRN_Rate = current.current_municipal_tax_RN;
    var prevRate = previousValue * previous.municipal.previous_municipal_tax_rate//previous.previous_municipal_tax_rate;
    var rate2018 = otherValue * taxRates.other_year.non_res.municipal.other_municipal_tax_rate;

    var btc = previous.business_tax_consolodation;
    var flow_through = current.flow_through;
    var rebate = current.current_municipal_rebate;
    var shift = current.current_municipal_revenue_shift;
    var ptp2019 = ((prevRate - btc) - (rate2018 * (0.9))) || 0;

    if(splitClass){
      if(percents.previous.non_res < 0.5 || percents.other.non_res < 0.5){
        ptp2019 = 0;
      } else {
        ptp2019 = ( ((previousValue * percents.previous.non_res) * (previous.municipal.previous_municipal_tax_rate - 0.0007411)) - ((otherValue * percents.other.non_res) * (taxRates.other_year.non_res.municipal.other_municipal_tax_rate * (0.9))) ) || 0;
      }
    }

    if (ptp2019 <= 25 || otherValue === 0) {
      ptp2019 = 0;
    } else {
      ptp2019 = parseFloat(ptp2019.toFixed(2));
    }


    var rates = (currentRN_Rate + currentRate + flow_through + rebate + shift);
    var curr = currentValue * rates;
    var prev = prevRate - ptp2019;

    if(splitClass){
      if(percents.current.non_res < 0.5 || percents.previous.non_res < 0.5){
        curr = null;
        prev = null;
      } else {
        curr = currentValue * percents.current.non_res * rates;
        prev = previousValue * percents.previous.non_res * previous.municipal.previous_municipal_tax_rate - ptp2019;
      }
    }

    var ptp2020 = curr - (prev * 1.1) || 0;

    console.log(curr, prev, currentValue, rates, prevRate, ptp2020, ptp2019)

    if(ptp2020 <= 25 || previousValue === 0){
      ptp2020 = 0;
    } else {
      ptp2020 = -Math.abs(ptp2020)
    }

    if(splitClass){
      split = 'split';
    } else {
      split = 'non_res';
    }

    taxRates.previous_year[split].last_year_PTP_credit = ptp2019;
    taxRates.previous_year[split].ptp_credit = ptp2019;

    taxRates.current_year[split].this_year_PTP_credit = ptp2020;
    taxRates.current_year[split].municipal.ptp_credit = ptp2020;

    return rates;

  }

  *//**
  make sure hte percentages add up to 100
  * @return {void};
  *//*
  function checkPercents(inputs) {
    var array = [];
    var obj = {};
    var percentTotal = 0;
    var i, value, type, input, initial;

    for (i = 0; i < inputs.length; i++) {
      //if (inputs[i].value) {
        input = inputs[i];
        value = input.value || 0;
        initial = parseFloat(value);
        value = parseFloat((value / 100).toFixed(4));
        type = input.getAttribute('data-type');
        percentTotal += initial;
        array.push({value: value, type: type});
        obj[type] = value;
    }
    if (percentTotal !== 100 || array.length === 0) {
      inputs[0].parentNode.classList.add('percent-error');
      return false;
    }else{
      inputs[0].parentNode.classList.remove('percent-error');
    }
    return obj;
  }


  *//**
  * update the dates on the page
  * @param {string} year - a year, eg. '2018'
  * @param {string} selector - a css selector (eg. '.tax-year')
  * @return {void};
  *//*
  function setYear(year, selector) {
    var yearElements = elements.container.querySelectorAll(selector),
      i;
    for (i = 0; i < yearElements.length; i++) {
      yearElements[i].innerHTML = year;
    }
  }

  *//**
  // check for number
  * @param {KeyboardEvent} event -
  * @return {false};
  *//*
  function isNumeric(event) {
    var key = event.key,
      notNumber = isNaN(key),
      keys = ['.', 'Shift', 'Escape', 'ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight', 'Enter', 'Tab', 'Control', 'Delete', 'Backspace'];
    if (keys.indexOf(key) > -1 || (event.ctrlKey && key === 'r')) {
      return true;
    }
    if (notNumber) {
      event.preventDefault();
    }
    return false;
  }

  *//**
  // check for percentage
  * @param {MouseEvent} event -
  * @return {false};
  *//*
  function isPercent(event) {
    var number = event.target.value,
      key = event.key,
      totalInput = parseFloat(number + key);
    if (isNaN(totalInput)) {
      return false;
    }
    if (totalInput > 0 && totalInput < 101) {
      return true;
    }
    event.preventDefault();
    return false;
  }

  *//**
  // format currency - add commas and decimal place
  * @param {number} value -
  * @return {string} - a number in currency format ($XX.XX);
  *//*
  function formatMoney(value) {
    var neg = '',
      val = value;
    if (val < 0) {
      val = val * -1;
      neg = '-';
    }
    return neg + '$' + val.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
  }

  *//**
  // add commas to a number
  * @param {number} value -
  * @return {string} - a number with commas
  *//*
  function commafy(value) {
    if(!value || value === ''){
      return 0;
    }
    value = value.toString();
    return value.replace(/(.)(?=(\d{3})+$)/g, '$1,');
  }

  *//**
  // remove commas from a number
  * @param {number} value -
  * @return {string} - a number
  *//*
  function digits(value) {
    if(!value || value === ''){
      return 0;
    }
    value = value.replace(/(,| )/g, '');
    return parseFloat(value);
  }

  *//**
  * add aria-hidden attributes to elements hidden by CSS display:none;
  * @param {HtmlElement} element - an html element
  * @return {void}
  *//*
  function setARIAs(element) {
    if (element.offsetParent === null) {
      element.setAttribute('aria-hidden', true);
    } else {
      element.setAttribute('aria-hidden', false);
    }
  }

  return {init: init};
})();

*/