(function ($) {
		
	COC.SearchResults = {	
		addTotalResultsToUrlForWebtrends: function() {

	        var params = window.location.search;
	        var regex = RegExp('(&W_srch_res=[0-9]+)+');
	        if (regex.test(params)) {
	            params = params.replace(regex, '');
	        }
	
	        var totalResults = $("div#total-results").text();
	        if(totalResults == '') {
	            totalResults = '0';
	        }
	        
			var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname;
			if(params === '') {
				newurl += '?W_srch_res=' + totalResults;
			}
			else {
				newurl += params + '&W_srch_res=' + totalResults;
			}
			
	        window.history.replaceState({}, "", newurl);        
		},
				
		copySearchTermToInput: function() {
				var searchTerm = document.getElementById("search-term").innerText;			
				document.getElementById('global-search-input').value = searchTerm;
			}
	}
	
	$(document).ready(function() {
		COC.SearchResults.addTotalResultsToUrlForWebtrends();
		COC.SearchResults.copySearchTermToInput();
	});

})(jQuery);