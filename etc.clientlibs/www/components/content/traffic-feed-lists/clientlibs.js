//console.log("file");
(function($) {


    COC.TrafficIncList = {

        GenerateTrafficIncList: function() {

            jQuery.ajax({
                url: 'https://gis.calgary.ca/arcgis/rest/services/pub_CalgaryDotCa/Events/MapServer/1/query?where=1%3D1&outFields=*&returnGeometry=false&f=geojson',
                type: "GET",
                contentType: "text/plain",
                dataType: "text",
                success: function(data) {
                	// Override with test data
                	// var data =  '{"type":"FeatureCollection","crs":{"type":"name","properties":{"name":"EPSG:4326"}},"features":[{"type":"Feature","id":2266759,"geometry":null,"properties":{"OBJECTID":2266759,"TITLE":" 17 Avenue and 100 Street SE ","DESCRIPTION":"Single vehicle incident. test.","START_DT_UTC":1604957350000,"END_DT_UTC":null,"MODIFIED_DT":1604932343000,"QUADRANT":"NW","REASONFORCLOSURE":null,"TRAFFICIMPACT":null,"ISDOWNTOWN":0}}]}';
                    var text = data;
                    var obj = JSON.parse(text);
                    
                    var dataSet = COC.TrafficIncList.TransformData(obj.features);
                    //console.log("Number of pulled INCs: " + obj.features.length);
                    //console.log(dataSet);
                    
                    
                    if (obj.features.length != 0) {
                    	
                    	 //$('#noaccidents').css('display', 'none');
                       //  $('#noaccidents').html('');
                         $('#INCLIST').css('display', 'block');

                         //$(".TrafficIncidents").children(".subtitle-block.cui").css("display", "none");
                         //$(".TrafficIncidents").children(".subtitle-block.cui").next().html("");

                         
                         
                    COC.TrafficIncList.createIncidentList(dataSet);
                    	 
                    	 
                    } else {
                       // $('#noaccidents').css('display', 'block');
                        //$('#noaccidents').html('<b>Most recent update returned 0 traffic incidents</b>');
                        $('#INCLIST').css('display', 'block');

                        $('#INCLIST').html('<b>Most recent update returned 0 traffic incidents</b>');

                        //$('#INCLIST').css('display', 'none');
                       // $(".TrafficIncidents").children(".subtitle-block.cui").css("display", "none");
                       // $(".TrafficIncidents").children(".subtitle-block.cui").next().html("");

                    }
                    
                    var today = new Date().toLocaleString();
                    //console.log('Last pull from ArcGIS :' + today);
                    $("#guidetime").html("<b>Last update: <time datetime="+ today +">"+today+"</time></b>");



                    



                },
                async: true
            });

            var today = new Date().toLocaleString();
            //console.log('Last pull from ArcGIS :' + today);
            $("#guidetime").text("<b>Last update: " + today +"</b>");

        },

        createIncidentList: function(dataSet) {
        	//console.log("inside Create function");
        	//console.log(dataSet);
           
        	var i;
            var internalDiv = "";

            var Title;
            var Description;
            var divIndicator;
            var startutc;
            var FormatedTime;
            var lastrow;


            
            for (i = 0; i < dataSet.length; i++) {
              
            	lastrow = "";

                Title = dataSet[i].Title;
                Description = dataSet[i].Description;
                startutc = dataSet[i].startutc;
                FormatedTime = COC.TrafficIncList.ConvertUTCDate(startutc);



                if (i + 1 == dataSet.length) {
                    lastrow += "";
                } else {
                    lastrow += "<hr aria-hidden='true'>";

                }
               
                internalDiv += "<p><b class='highlight-title'>" + Title + "</b><br><b>Date:</b> " + FormatedTime + "<br><b>Details:</b><br>" + Description + "</p>" + lastrow;


            }

            $('#INCLIST').html(internalDiv);
            
        



        },

        ConvertUTCDate: function(startutc) {
            if (startutc != "") {
                var week = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                var monthArray = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];                
                var date = new Date(startutc);
                var IncDate = date.toString();
                var d = new Date(startutc);
                var Day = d.getDate();
          	  	var weekday = week[d.getDay()];             	                
                var Year = d.getFullYear();
                var Month = d.getMonth();
                var time = d.toLocaleTimeString().replace(/:\d{2}\s/, ' ');
                var FormatedTime = weekday + ", " + monthArray[Month] + " " + Day + ", " + Year + " at " + time;
            } else {
                var FormatedTime = "N/A";

            }
            
            return FormatedTime;
        },

        TransformData: function(trafficIncidentList) {

            var dataSet = [];
            var i;
            var record;
            var globalID;
            var Description;
            var Title;
            var startutc;
            var endutc;
            var quadrant;
            var Extractedquardent;
            var n = 2;
            var cleantitle;
            var lastword;

            for (i = 0; i < trafficIncidentList.length; i++) { //features[""0""].properties.DESCRIPTION
                globalID = trafficIncidentList[i].properties.GlobalID || "";
                Description = trafficIncidentList[i].properties.DESCRIPTION || "";
                Title = trafficIncidentList[i].properties.TITLE || "";
                startutc = trafficIncidentList[i].properties.START_DT_UTC || "";
                endutc = trafficIncidentList[i].properties.END_DT_UTCL || "";
               

                record = {

                    "globalID": globalID,
                    "Description": Description,
                    "Title": Title,
                    "startutc": startutc,
                    "endutc": endutc,

                };

                dataSet.push(record);
            }
            return dataSet;


        },

        ManualCall: function() {
            var paramList = "";
            var JSONBODY = '{"type":"FeatureCollection","crs":{"type":"name","properties":{"name":"EPSG:4326"}},"features":[{"type":"Feature","id":2105322,"geometry":null,"properties":{"OBJECTID":2105322,"TITLE":" Chaparral Boulevard and Chaparral Ridge Drive NE","DESCRIPTION":"Two vehicle incident.","START_DT_UTC":1596049071000,"END_DT_UTC":null}},{"type":"Feature","id":2105318,"geometry":null,"properties":{"OBJECTID":2105318,"TITLE":"John Laurie Blvd / 14 Street SE ","DESCRIPTION":"Two vehicle incident. Blocking the right lane","START_DT_UTC":1596044312000,"END_DT_UTC":null}},{"type":"Feature","id":2105318,"geometry":null,"properties":{"OBJECTID":2105318,"TITLE":" Southbound Shaganappi Trail and Crowchild Trail SE ","DESCRIPTION":"Two vehicle incident. Blocking the right lane","START_DT_UTC":1596044312000,"END_DT_UTC":null}}]}';
             var JSONBODY = '{"type":"FeatureCollection","crs":{"type":"name","properties":{"name":"EPSG:4326"}},"features":[]}';
            // var JSONBODY = '{"type":"FeatureCollection","crs":{"type":"name","properties":{"name":"EPSG:4326"}},"features":[{"type":"Feature","id":2109453,"geometry":null,"properties":{"OBJECTID":2109453,"TITLE":" 60 Street N ","DESCRIPTION":"Traffic incident.","START_DT_UTC":1596145283000,"END_DT_UTC":null}},{"type":"Feature","id":2109454,"geometry":null,"properties":{"OBJECTID":2109454,"TITLE":" Eastbound Glenmore Trail at Blackfoot Trail SE ","DESCRIPTION":"Two vehicle incident.","START_DT_UTC":1596144886000,"END_DT_UTC":null}},{"type":"Feature","id":2109455,"geometry":null,"properties":{"OBJECTID":2109455,"TITLE":" Westbound Anderson Road at Macleod Trail SE","DESCRIPTION":"Traffic incident.   Blocking the right lanes and northbound left turn bay The lights are blank","START_DT_UTC":1596142827000,"END_DT_UTC":null}},{"type":"Feature","id":2109456,"geometry":null,"properties":{"OBJECTID":2109456,"TITLE":" Eastbound 16 Avenue approaching Valley Ridge Boulevard NW ","DESCRIPTION":"Traffic incident.","START_DT_UTC":1596142069000,"END_DT_UTC":null}}]}';
            //var JSONBODY = '{"type":"FeatureCollection","crs":{"type":"name","properties":{"name":"EPSG:4326"}},"features":[{"type":"Feature","id":2109453,"geometry":null,"properties":{"OBJECTID":2109453,"TITLE":" 60 Street downtown ","DESCRIPTION":"Traffic incident.","START_DT_UTC":1596145283000,"END_DT_UTC":null}},{"type":"Feature","id":2112614,"geometry":null,"properties":{"OBJECTID":2112614,"TITLE":" Northbound Deerfoot Trail approaching Southland Drive Se","DESCRIPTION":"Single vehicle incident. Blocking the right lane","START_DT_UTC":1596212350000,"END_DT_UTC":null}},{"type":"Feature","id":2112615,"geometry":null,"properties":{"OBJECTID":2112615,"TITLE":" Eastbound 5 Avenue and Macleod Trail SE ","DESCRIPTION":"Single vehicle incident. Blocking multiple lanes","START_DT_UTC":1596211674000,"END_DT_UTC":null}}]}';
            // var JSONBODY = '{"type":"FeatureCollection","crs":{"type":"name","properties":{"name":"EPSG:4326"}},"features":[{"type":"Feature","id":2112617,"geometry":null,"properties":{"OBJECTID":2112617,"TITLE":" Symons Valley Road and 144 Avenue NW ","DESCRIPTION":"Traffic incident.","START_DT_UTC":1596215743000,"END_DT_UTC":null}},{"type":"Feature","id":2112618,"geometry":null,"properties":{"OBJECTID":2112618,"TITLE":" Eastbound 5 Avenue and Macleod Trail SE ","DESCRIPTION":"Single vehicle incident. Blocking multiple lanes","START_DT_UTC":1596211674000,"END_DT_UTC":null}}]}';
            // var JSONBODY = '{"type":"FeatureCollection","crs":{"type":"name","properties":{"name":"EPSG:4326"}},"features":[{"type":"Feature","id":2112841,"geometry":null,"properties":{"OBJECTID":2112841,"TITLE":" Rocky Ridge Road and Country Hills Boulevard NW ","DESCRIPTION":"Traffic incident.","START_DT_UTC":1596217182000,"END_DT_UTC":null}},{"type":"Feature","id":2112842,"geometry":null,"properties":{"OBJECTID":2112842,"TITLE":" Symons Valley Road and 144 Avenue NW ","DESCRIPTION":"Traffic incident.","START_DT_UTC":1596215743000,"END_DT_UTC":null}}]}'; 
           // var JSONBODY = '{"type":"FeatureCollection","crs":{"type":"name","properties":{"name":"EPSG:4326"}},"features":[{"type":"Feature","id":2115844,"geometry":null,"properties":{"OBJECTID":2115844,"TITLE":" Downtown Area ","DESCRIPTION":"Large gathering in progress.","START_DT_UTC":1596749706000,"END_DT_UTC":null}}]}';
//var JSONBODY = '{"type":"FeatureCollection","crs":{"type":"name","properties":{"name":"EPSG:4326"}},"features":[{"type":"Feature","id":2125350,"geometry":null,"properties":{"OBJECTID":2125350,"TITLE":" 56 Street and 23 Avenue NE ","DESCRIPTION":"Traffic incident.","START_DT_UTC":1597339298000,"END_DT_UTC":null}},{"type":"Feature","id":2125351,"geometry":null,"properties":{"OBJECTID":2125351,"TITLE":" 31 Street and 17 Avenue SE ","DESCRIPTION":"There is an incident involving a pedestrian.","START_DT_UTC":1597338453000,"END_DT_UTC":1597338453000}}]}';
            var text = JSONBODY;
            var obj = JSON.parse(text);
            
           
            
            var dataSet = COC.TrafficIncList.TransformData(obj.features);
            //console.log("Number of pulled INCs: " + obj.features.length);
            //console.log(dataSet);
            
            
            if (obj.features.length != 0) {
                //var arrangedDataset = COC.TrafficIncList.groupBy(dataSet, 'quadrant');
              //  //console.log("arrangedDataset");

               // //console.log(arrangedDataset);
               // COC.TrafficIncList.createIncidentList(arrangedDataset);
                $('#INCLIST').css('display', 'block');

            	 COC.TrafficIncList.createIncidentList(dataSet);
            	 
            	 
            } else {
                //$('#noaccidents').css('display', 'block');
               // $('#noaccidents').html('<b>Most recent update returned 0 traffic incidents</b>');
               // $(".TrafficIncidents").children(".subtitle-block.cui").css("display", "none");
               // $(".TrafficIncidents").children(".subtitle-block.cui").next().html("");
                $('#INCLIST').css('display', 'block');

                $('#INCLIST').html('<b>Most recent update returned 0 traffic incidents</b>');

            }
            
            var today = new Date().toLocaleString();
            //console.log('Last pull from ArcGIS :' + today);
            $("#guidetime").html("<b>Last update: <time datetime="+ today +">"+today+"</time></b>");



        },



    }


})(jQuery);
(function($) {

    COC.ConstructionDetoursList = {


        GenerateConstructionDetoursList: function() {


            jQuery.ajax({
            	//All areas
               url: 'https://gis.calgary.ca/arcgis/rest/services/pub_CalgaryDotCa/Events/MapServer/0/query?where=1%3D1&outFields=*&returnGeometry=false&f=geojson&orderByFields=TITLE',
              //NW only
             // url: 'https://gis.calgary.ca/arcgis/rest/services/pub_CalgaryDotCa/Events/MapServer/0/query?where=TITLE+LIKE%27%25+NW%27&outFields=*&returnGeometry=false&f=geojson&orderByFields=TITLE',

                contentType: "text/plain",
                dataType: "text",
                success: function(data) {
                    var text = data;
                    var obj = JSON.parse(text);
                    var dataSet = COC.ConstructionDetoursList.TransformData(obj.features);
                    var arrangedDataset = COC.ConstructionDetoursList.groupBy(dataSet, 'quadrant');
                    COC.ConstructionDetoursList.createConstructionDetoursList(arrangedDataset);
                },
                async: true
            });

            var start = new Date;
            var currentGMT = start.getUTCHours(); 
            var d = new Date();
            var Day = d.getDate();
            var Year = d.getFullYear();
            var Month = d.getMonth() + 1;

            if (currentGMT >= 21) {
                var UTCTime = Month + "/" + Day + "/" + Year + "," + " 3:00:00 PM";
            }
            if (currentGMT < 21 && currentGMT >= 9) {
                var UTCTime = Month + "/" + Day + "/" + Year + "," + " 3:00:00 AM";
            }

            $('#detourslistguidetime').html("<b>Last update: " + UTCTime+"</b>");
        
        },

        createConstructionDetoursList: function(dataSet) {
        	
        	var detourDiv = "";
            var downtownDiv = "";
        	var quadrantDiv = "";
            var Title;
            var Description;
            var divIndicator;
            var startutc;
            var StartTime;
            var EndTime;
            var isDowntown;
            var lastrow;
            
            $.each(dataSet, function(key, value) {
                quadrantDiv = "";
                divIndicator = key.replace(/(^\s+|\s+$)/g, '');

                if (dataSet.hasOwnProperty('other')) {
                    if (dataSet.hasOwnProperty('SE') || dataSet.hasOwnProperty('SW') || dataSet.hasOwnProperty('NW') ||
                        dataSet.hasOwnProperty('NE') || dataSet.hasOwnProperty('S') || dataSet.hasOwnProperty('N') || dataSet.hasOwnProperty('E') ||
                        dataSet.hasOwnProperty('W')) {
                        $(".ConstructionDetoursothertitle").css("display", "block");
                        $("#ConstructionDetourscheckother").html("Other");

                    } else {
                        $(".ConstructionDetoursothertitle").css("display", "none");
                    }

                } else {
                    //console.log ("N/A quadrant exception");
                }                

                $('.' + divIndicator + 'titledetours').css('display', 'block');
                for (var i = 0; i < value.length; i++) {
                    lastrow = "";

                    Title = value[i].Title;
                    Description = value[i].Description;
                    startutc = value[i].startutc;
                    endutc = value[i].endutc;
                    isDowntown = value[i].isDowntown;     
                    
                    StartTime = COC.ConstructionDetoursList.ConvertUTCDate(startutc);
                    EndTime = COC.ConstructionDetoursList.ConvertUTCDate(endutc);
                    
                    if (i + 1 == value.length) {
                        lastrow += "";
                    } else {
                        lastrow += "<hr aria-hidden='true'>";

                    }
                    
                    detourDiv = "<p><b class='highlight-title'>" + Title + "</b><br>" +
                    				"<b>Start:</b> " + StartTime + "<br>" + 
                    				"<b>End:</b> " + EndTime + "<br>" +
                    				"<b>Traffic Impacts:</b> " + value[i].trafficImpact + "<br>" +
                    				"<b>Reason for Closure:</b> " + value[i].reasonForClosure + "<br>" + 
                    				"<b>Area:</b> " + value[i].quadrant + "<br>" +
                    				"<b>Last Modified:</b> " + COC.ConstructionDetoursList.ConvertUTCDate(value[i].lastModifiedUTC);
                    
                    // Add to Quadrant accordion
                    quadrantDiv += detourDiv;
                    
                    // Add to Downtown accordion if appropriate
                    if(isDowntown) {
                    	downtownDiv += detourDiv;
                    }                                        
                }

                $('#' + divIndicator + 'detours').html(quadrantDiv);  
                
                if(downtownDiv != "") {
                	$('#DTdetours').html(downtownDiv);
                }                
            });

            $("#preload-placeholder").css("display","none");
            $("#ScheduledRoadClosuresAccordion").css("display","block");
        },


        groupBy: function(objectArray, property) {

            if (objectArray.length == 0) {
                return;
            }

            return objectArray.reduce(function (acc, obj) {
                var key = obj[property];
                if (!acc[key]) {
                    acc[key] = [];
                }
                // Add object to list for given key's value
                acc[key].push(obj);
                return acc;
            }, {});
        },

        ConvertUTCDate: function(startutc) {
            if (startutc != "") {
                var week = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                var monthArray = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];                
                var date = new Date(startutc);
                var IncDate = date.toString();
                var d = new Date(startutc);
                var Day = d.getDate();
          	  	var weekday = week[d.getDay()];             	                
                var Year = d.getFullYear();
                var Month = d.getMonth();
                var time = d.toLocaleTimeString().replace(/:\d{2}\s/, ' ');
                var FormatedTime = weekday + ", " + monthArray[Month] + " " + Day + ", " + Year + " at " + time;
            } else {
                var FormatedTime = "N/A";

            }
            
            return FormatedTime;
        },

        TransformData: function(ConstructionDetoursList) {

            var dataSet = [];
            var i;
            var j;
            var record;
            var globalID;
            var Description;
            var Title;
            var startutc;
            var endutc;
            var quadrant;
            var Extractedquardent;
            var n = 2;
            var cleantitle;
            var lastword;
            var direction = ["NW", "NE", "SW", "SE"];
            var trafficImpact;
			var reasonForClosure; 
			var quadrant;
			var isDowntown;
			var lastModifiedUTC; 
			            
            for (i = 0; i < ConstructionDetoursList.length; i++) { //[""0""].attributes.Description //features[""0""].properties.DESCRIPTION
                globalID = ConstructionDetoursList[i].properties.GlobalID || "";
                Description = ConstructionDetoursList[i].properties.DESCRIPTION || "";
                Title = ConstructionDetoursList[i].properties.TITLE || "";
                startutc = ConstructionDetoursList[i].properties.START_DT_UTC || "";
                endutc = ConstructionDetoursList[i].properties.END_DT_UTC || "";
                trafficImpact = ConstructionDetoursList[i].properties.TRAFFICIMPACT || "N/A";
                reasonForClosure = ConstructionDetoursList[i].properties.REASONFORCLOSURE || "Road construction";
                isDowntown = ConstructionDetoursList[i].properties.ISDOWNTOWN || "";
                lastModifiedUTC = ConstructionDetoursList[i].properties.MODIFIED_DT_UTC || "";
                
                cleantitle = $.trim(Title);
               
                lastword = cleantitle.split(" ").pop();

                for (j = 0; j < direction.length; j++)
            	{
                	var n = Title.search(direction[j]);
                	
                	   if (n!= -1)
                	  {
                		   quadrant = direction[j];
                		   
                		   record = {
                                   "globalID": 			globalID,
                                   "Description": 		Description,
                                   "Title": 			Title,
                                   "startutc": 			startutc,
                                   "endutc": 			endutc,
                                   "quadrant": 			quadrant,
                                   "trafficImpact": 	trafficImpact,
                                   "reasonForClosure": 	reasonForClosure,
                                   "isDowntown":		isDowntown,
                                   "lastModifiedUTC": 	lastModifiedUTC,
                               };
                		   dataSet.push(record);
                		   

                	  }
            	}
            }
             
            return dataSet;

        },
     
    }


})(jQuery);
