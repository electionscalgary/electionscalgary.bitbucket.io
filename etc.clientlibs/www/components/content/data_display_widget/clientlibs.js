/*!***************************************************
* mark.js v9.0.0
* https://markjs.io/
* Copyright (c) 2014–2018, Julian Kühnel
* Released under the MIT license https://git.io/vwTVl
*****************************************************/
!function(e,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t(require("jquery")):"function"==typeof define&&define.amd?define(["jquery"],t):e.Mark=t(e.jQuery)}(this,function(e){"use strict";function t(e){return(t="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e})(e)}function n(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function r(e,t){for(var n=0;n<t.length;n++){var r=t[n];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),Object.defineProperty(e,r.key,r)}}function o(e,t,n){return t&&r(e.prototype,t),n&&r(e,n),e}function i(){return(i=Object.assign||function(e){for(var t=1;t<arguments.length;t++){var n=arguments[t];for(var r in n)Object.prototype.hasOwnProperty.call(n,r)&&(e[r]=n[r])}return e}).apply(this,arguments)}e=e&&e.hasOwnProperty("default")?e.default:e;var a=
/* */
function(){function e(t){var r=!(arguments.length>1&&void 0!==arguments[1])||arguments[1],o=arguments.length>2&&void 0!==arguments[2]?arguments[2]:[],i=arguments.length>3&&void 0!==arguments[3]?arguments[3]:5e3;n(this,e),this.ctx=t,this.iframes=r,this.exclude=o,this.iframesTimeout=i}return o(e,[{key:"getContexts",value:function(){var e=[];return(void 0!==this.ctx&&this.ctx?NodeList.prototype.isPrototypeOf(this.ctx)?Array.prototype.slice.call(this.ctx):Array.isArray(this.ctx)?this.ctx:"string"==typeof this.ctx?Array.prototype.slice.call(document.querySelectorAll(this.ctx)):[this.ctx]:[]).forEach(function(t){var n=e.filter(function(e){return e.contains(t)}).length>0;-1!==e.indexOf(t)||n||e.push(t)}),e}},{key:"getIframeContents",value:function(e,t){var n,r=arguments.length>2&&void 0!==arguments[2]?arguments[2]:function(){};try{var o=e.contentWindow;if(n=o.document,!o||!n)throw new Error("iframe inaccessible")}catch(e){r()}n&&t(n)}},{key:"isIframeBlank",value:function(e){var t="about:blank",n=e.getAttribute("src").trim();return e.contentWindow.location.href===t&&n!==t&&n}},{key:"observeIframeLoad",value:function(e,t,n){var r=this,o=!1,i=null,a=function a(){if(!o){o=!0,clearTimeout(i);try{r.isIframeBlank(e)||(e.removeEventListener("load",a),r.getIframeContents(e,t,n))}catch(e){n()}}};e.addEventListener("load",a),i=setTimeout(a,this.iframesTimeout)}},{key:"onIframeReady",value:function(e,t,n){try{"complete"===e.contentWindow.document.readyState?this.isIframeBlank(e)?this.observeIframeLoad(e,t,n):this.getIframeContents(e,t,n):this.observeIframeLoad(e,t,n)}catch(e){n()}}},{key:"waitForIframes",value:function(e,t){var n=this,r=0;this.forEachIframe(e,function(){return!0},function(e){r++,n.waitForIframes(e.querySelector("html"),function(){--r||t()})},function(e){e||t()})}},{key:"forEachIframe",value:function(t,n,r){var o=this,i=arguments.length>3&&void 0!==arguments[3]?arguments[3]:function(){},a=t.querySelectorAll("iframe"),s=a.length,c=0;a=Array.prototype.slice.call(a);var u=function(){--s<=0&&i(c)};s||u(),a.forEach(function(t){e.matches(t,o.exclude)?u():o.onIframeReady(t,function(e){n(t)&&(c++,r(e)),u()},u)})}},{key:"createIterator",value:function(e,t,n){return document.createNodeIterator(e,t,n,!1)}},{key:"createInstanceOnIframe",value:function(t){return new e(t.querySelector("html"),this.iframes)}},{key:"compareNodeIframe",value:function(e,t,n){if(e.compareDocumentPosition(n)&Node.DOCUMENT_POSITION_PRECEDING){if(null===t)return!0;if(t.compareDocumentPosition(n)&Node.DOCUMENT_POSITION_FOLLOWING)return!0}return!1}},{key:"getIteratorNode",value:function(e){var t=e.previousNode();return{prevNode:t,node:null===t?e.nextNode():e.nextNode()&&e.nextNode()}}},{key:"checkIframeFilter",value:function(e,t,n,r){var o=!1,i=!1;return r.forEach(function(e,t){e.val===n&&(o=t,i=e.handled)}),this.compareNodeIframe(e,t,n)?(!1!==o||i?!1===o||i||(r[o].handled=!0):r.push({val:n,handled:!0}),!0):(!1===o&&r.push({val:n,handled:!1}),!1)}},{key:"handleOpenIframes",value:function(e,t,n,r){var o=this;e.forEach(function(e){e.handled||o.getIframeContents(e.val,function(e){o.createInstanceOnIframe(e).forEachNode(t,n,r)})})}},{key:"iterateThroughNodes",value:function(e,t,n,r,o){for(var i,a,s,c=this,u=this.createIterator(t,e,r),l=[],h=[];s=void 0,s=c.getIteratorNode(u),a=s.prevNode,i=s.node;)this.iframes&&this.forEachIframe(t,function(e){return c.checkIframeFilter(i,a,e,l)},function(t){c.createInstanceOnIframe(t).forEachNode(e,function(e){return h.push(e)},r)}),h.push(i);h.forEach(function(e){n(e)}),this.iframes&&this.handleOpenIframes(l,e,n,r),o()}},{key:"forEachNode",value:function(e,t,n){var r=this,o=arguments.length>3&&void 0!==arguments[3]?arguments[3]:function(){},i=this.getContexts(),a=i.length;a||o(),i.forEach(function(i){var s=function(){r.iterateThroughNodes(e,i,t,n,function(){--a<=0&&o()})};r.iframes?r.waitForIframes(i,s):s()})}}],[{key:"matches",value:function(e,t){var n="string"==typeof t?[t]:t,r=e.matches||e.matchesSelector||e.msMatchesSelector||e.mozMatchesSelector||e.oMatchesSelector||e.webkitMatchesSelector;if(r){var o=!1;return n.every(function(t){return!r.call(e,t)||(o=!0,!1)}),o}return!1}}]),e}(),s=
/* */
function(){function e(t){n(this,e),this.opt=i({},{diacritics:!0,synonyms:{},accuracy:"partially",caseSensitive:!1,ignoreJoiners:!1,ignorePunctuation:[],wildcards:"disabled"},t)}return o(e,[{key:"create",value:function(e){return"disabled"!==this.opt.wildcards&&(e=this.setupWildcardsRegExp(e)),e=this.escapeStr(e),Object.keys(this.opt.synonyms).length&&(e=this.createSynonymsRegExp(e)),(this.opt.ignoreJoiners||this.opt.ignorePunctuation.length)&&(e=this.setupIgnoreJoinersRegExp(e)),this.opt.diacritics&&(e=this.createDiacriticsRegExp(e)),e=this.createMergedBlanksRegExp(e),(this.opt.ignoreJoiners||this.opt.ignorePunctuation.length)&&(e=this.createJoinersRegExp(e)),"disabled"!==this.opt.wildcards&&(e=this.createWildcardsRegExp(e)),e=this.createAccuracyRegExp(e),new RegExp(e,"gm".concat(this.opt.caseSensitive?"":"i"))}},{key:"sortByLength",value:function(e){return e.sort(function(e,t){return e.length===t.length?e>t?1:-1:t.length-e.length})}},{key:"escapeStr",value:function(e){return e.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g,"\\$&")}},{key:"createSynonymsRegExp",value:function(e){var t=this,n=this.opt.synonyms,r=this.opt.caseSensitive?"":"i",o=this.opt.ignoreJoiners||this.opt.ignorePunctuation.length?"\0":"";for(var i in n)if(n.hasOwnProperty(i)){var a=Array.isArray(n[i])?n[i]:[n[i]];a.unshift(i),(a=this.sortByLength(a).map(function(e){return"disabled"!==t.opt.wildcards&&(e=t.setupWildcardsRegExp(e)),e=t.escapeStr(e)}).filter(function(e){return""!==e})).length>1&&(e=e.replace(new RegExp("(".concat(a.map(function(e){return t.escapeStr(e)}).join("|"),")"),"gm".concat(r)),o+"(".concat(a.map(function(e){return t.processSynonyms(e)}).join("|"),")")+o))}return e}},{key:"processSynonyms",value:function(e){return(this.opt.ignoreJoiners||this.opt.ignorePunctuation.length)&&(e=this.setupIgnoreJoinersRegExp(e)),e}},{key:"setupWildcardsRegExp",value:function(e){return(e=e.replace(/(?:\\)*\?/g,function(e){return"\\"===e.charAt(0)?"?":""})).replace(/(?:\\)*\*/g,function(e){return"\\"===e.charAt(0)?"*":""})}},{key:"createWildcardsRegExp",value:function(e){var t="withSpaces"===this.opt.wildcards;return e.replace(/\u0001/g,t?"[\\S\\s]?":"\\S?").replace(/\u0002/g,t?"[\\S\\s]*?":"\\S*")}},{key:"setupIgnoreJoinersRegExp",value:function(e){return e.replace(/[^(|)\\]/g,function(e,t,n){var r=n.charAt(t+1);return/[(|)\\]/.test(r)||""===r?e:e+"\0"})}},{key:"createJoinersRegExp",value:function(e){var t=[],n=this.opt.ignorePunctuation;return Array.isArray(n)&&n.length&&t.push(this.escapeStr(n.join(""))),this.opt.ignoreJoiners&&t.push("\\u00ad\\u200b\\u200c\\u200d"),t.length?e.split(/\u0000+/).join("[".concat(t.join(""),"]*")):e}},{key:"createDiacriticsRegExp",value:function(e){var t=this.opt.caseSensitive?"":"i",n=this.opt.caseSensitive?["aàáảãạăằắẳẵặâầấẩẫậäåāą","AÀÁẢÃẠĂẰẮẲẴẶÂẦẤẨẪẬÄÅĀĄ","cçćč","CÇĆČ","dđď","DĐĎ","eèéẻẽẹêềếểễệëěēę","EÈÉẺẼẸÊỀẾỂỄỆËĚĒĘ","iìíỉĩịîïī","IÌÍỈĨỊÎÏĪ","lł","LŁ","nñňń","NÑŇŃ","oòóỏõọôồốổỗộơởỡớờợöøō","OÒÓỎÕỌÔỒỐỔỖỘƠỞỠỚỜỢÖØŌ","rř","RŘ","sšśșş","SŠŚȘŞ","tťțţ","TŤȚŢ","uùúủũụưừứửữựûüůū","UÙÚỦŨỤƯỪỨỬỮỰÛÜŮŪ","yýỳỷỹỵÿ","YÝỲỶỸỴŸ","zžżź","ZŽŻŹ"]:["aàáảãạăằắẳẵặâầấẩẫậäåāąAÀÁẢÃẠĂẰẮẲẴẶÂẦẤẨẪẬÄÅĀĄ","cçćčCÇĆČ","dđďDĐĎ","eèéẻẽẹêềếểễệëěēęEÈÉẺẼẸÊỀẾỂỄỆËĚĒĘ","iìíỉĩịîïīIÌÍỈĨỊÎÏĪ","lłLŁ","nñňńNÑŇŃ","oòóỏõọôồốổỗộơởỡớờợöøōOÒÓỎÕỌÔỒỐỔỖỘƠỞỠỚỜỢÖØŌ","rřRŘ","sšśșşSŠŚȘŞ","tťțţTŤȚŢ","uùúủũụưừứửữựûüůūUÙÚỦŨỤƯỪỨỬỮỰÛÜŮŪ","yýỳỷỹỵÿYÝỲỶỸỴŸ","zžżźZŽŻŹ"],r=[];return e.split("").forEach(function(o){n.every(function(n){if(-1!==n.indexOf(o)){if(r.indexOf(n)>-1)return!1;e=e.replace(new RegExp("[".concat(n,"]"),"gm".concat(t)),"[".concat(n,"]")),r.push(n)}return!0})}),e}},{key:"createMergedBlanksRegExp",value:function(e){return e.replace(/[\s]+/gim,"[\\s]+")}},{key:"createAccuracyRegExp",value:function(e){var t=this,n=this.opt.accuracy,r="string"==typeof n?n:n.value,o="string"==typeof n?[]:n.limiters,i="";switch(o.forEach(function(e){i+="|".concat(t.escapeStr(e))}),r){case"partially":default:return"()(".concat(e,")");case"complementary":return i="\\s"+(i||this.escapeStr("!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~¡¿")),"()([^".concat(i,"]*").concat(e,"[^").concat(i,"]*)");case"exactly":return"(^|\\s".concat(i,")(").concat(e,")(?=$|\\s").concat(i,")")}}}]),e}(),c=
/* */
function(){function e(t){n(this,e),this.ctx=t,this.ie=!1;var r=window.navigator.userAgent;(r.indexOf("MSIE")>-1||r.indexOf("Trident")>-1)&&(this.ie=!0)}return o(e,[{key:"log",value:function(e){var n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:"debug",r=this.opt.log;this.opt.debug&&"object"===t(r)&&"function"==typeof r[n]&&r[n]("mark.js: ".concat(e))}},{key:"getSeparatedKeywords",value:function(e){var t=this,n=[];return e.forEach(function(e){t.opt.separateWordSearch?e.split(" ").forEach(function(e){e.trim()&&-1===n.indexOf(e)&&n.push(e)}):e.trim()&&-1===n.indexOf(e)&&n.push(e)}),{keywords:n.sort(function(e,t){return t.length-e.length}),length:n.length}}},{key:"isNumeric",value:function(e){return Number(parseFloat(e))==e}},{key:"checkRanges",value:function(e){var t=this;if(!Array.isArray(e)||"[object Object]"!==Object.prototype.toString.call(e[0]))return this.log("markRanges() will only accept an array of objects"),this.opt.noMatch(e),[];var n=[],r=0;return e.sort(function(e,t){return e.start-t.start}).forEach(function(e){var o=t.callNoMatchOnInvalidRanges(e,r),i=o.start,a=o.end;o.valid&&(e.start=i,e.length=a-i,n.push(e),r=a)}),n}},{key:"callNoMatchOnInvalidRanges",value:function(e,t){var n,r,o=!1;return e&&void 0!==e.start?(r=(n=parseInt(e.start,10))+parseInt(e.length,10),this.isNumeric(e.start)&&this.isNumeric(e.length)&&r-t>0&&r-n>0?o=!0:(this.log("Ignoring invalid or overlapping range: "+"".concat(JSON.stringify(e))),this.opt.noMatch(e))):(this.log("Ignoring invalid range: ".concat(JSON.stringify(e))),this.opt.noMatch(e)),{start:n,end:r,valid:o}}},{key:"checkWhitespaceRanges",value:function(e,t,n){var r,o=!0,i=n.length,a=t-i,s=parseInt(e.start,10)-a;return(r=(s=s>i?i:s)+parseInt(e.length,10))>i&&(r=i,this.log("End range automatically set to the max value of ".concat(i))),s<0||r-s<0||s>i||r>i?(o=!1,this.log("Invalid range: ".concat(JSON.stringify(e))),this.opt.noMatch(e)):""===n.substring(s,r).replace(/\s+/g,"")&&(o=!1,this.log("Skipping whitespace only range: "+JSON.stringify(e)),this.opt.noMatch(e)),{start:s,end:r,valid:o}}},{key:"getTextNodes",value:function(e){var t=this,n="",r=[];this.iterator.forEachNode(NodeFilter.SHOW_TEXT,function(e){r.push({start:n.length,end:(n+=e.textContent).length,node:e})},function(e){return t.matchesExclude(e.parentNode)?NodeFilter.FILTER_REJECT:NodeFilter.FILTER_ACCEPT},function(){e({value:n,nodes:r})})}},{key:"matchesExclude",value:function(e){return a.matches(e,this.opt.exclude.concat(["script","style","title","head","html"]))}},{key:"wrapRangeInTextNode",value:function(e,t,n){var r=this.opt.element?this.opt.element:"mark",o=e.splitText(t),i=o.splitText(n-t),a=document.createElement(r);return a.setAttribute("data-markjs","true"),this.opt.className&&a.setAttribute("class",this.opt.className),a.textContent=o.textContent,o.parentNode.replaceChild(a,o),i}},{key:"wrapRangeInMappedTextNode",value:function(e,t,n,r,o){var i=this;e.nodes.every(function(a,s){var c=e.nodes[s+1];if(void 0===c||c.start>t){if(!r(a.node))return!1;var u=t-a.start,l=(n>a.end?a.end:n)-a.start,h=e.value.substr(0,a.start),f=e.value.substr(l+a.start);if(a.node=i.wrapRangeInTextNode(a.node,u,l),e.value=h+f,e.nodes.forEach(function(t,n){n>=s&&(e.nodes[n].start>0&&n!==s&&(e.nodes[n].start-=l),e.nodes[n].end-=l)}),n-=l,o(a.node.previousSibling,a.start),!(n>a.end))return!1;t=a.end}return!0})}},{key:"wrapGroups",value:function(e,t,n,r){return r((e=this.wrapRangeInTextNode(e,t,t+n)).previousSibling),e}},{key:"separateGroups",value:function(e,t,n,r,o){for(var i=t.length,a=1;a<i;a++){var s=e.textContent.indexOf(t[a]);t[a]&&s>-1&&r(t[a],e)&&(e=this.wrapGroups(e,s,t[a].length,o))}return e}},{key:"wrapMatches",value:function(e,t,n,r,o){var i=this,a=0===t?0:t+1;this.getTextNodes(function(t){t.nodes.forEach(function(t){var o;for(t=t.node;null!==(o=e.exec(t.textContent))&&""!==o[a];){if(i.opt.separateGroups)t=i.separateGroups(t,o,a,n,r);else{if(!n(o[a],t))continue;var s=o.index;if(0!==a)for(var c=1;c<a;c++)s+=o[c].length;t=i.wrapGroups(t,s,o[a].length,r)}e.lastIndex=0}}),o()})}},{key:"wrapMatchesAcrossElements",value:function(e,t,n,r,o){var i=this,a=0===t?0:t+1;this.getTextNodes(function(t){for(var s;null!==(s=e.exec(t.value))&&""!==s[a];){var c=s.index;if(0!==a)for(var u=1;u<a;u++)c+=s[u].length;var l=c+s[a].length;i.wrapRangeInMappedTextNode(t,c,l,function(e){return n(s[a],e)},function(t,n){e.lastIndex=n,r(t)})}o()})}},{key:"wrapRangeFromIndex",value:function(e,t,n,r){var o=this;this.getTextNodes(function(i){var a=i.value.length;e.forEach(function(e,r){var s=o.checkWhitespaceRanges(e,a,i.value),c=s.start,u=s.end;s.valid&&o.wrapRangeInMappedTextNode(i,c,u,function(n){return t(n,e,i.value.substring(c,u),r)},function(t){n(t,e)})}),r()})}},{key:"unwrapMatches",value:function(e){for(var t=e.parentNode,n=document.createDocumentFragment();e.firstChild;)n.appendChild(e.removeChild(e.firstChild));t.replaceChild(n,e),this.ie?this.normalizeTextNode(t):t.normalize()}},{key:"normalizeTextNode",value:function(e){if(e){if(3===e.nodeType)for(;e.nextSibling&&3===e.nextSibling.nodeType;)e.nodeValue+=e.nextSibling.nodeValue,e.parentNode.removeChild(e.nextSibling);else this.normalizeTextNode(e.firstChild);this.normalizeTextNode(e.nextSibling)}}},{key:"markRegExp",value:function(e,t){var n=this;this.opt=t,this.log('Searching with expression "'.concat(e,'"'));var r=0,o="wrapMatches";this.opt.acrossElements&&(o="wrapMatchesAcrossElements"),this[o](e,this.opt.ignoreGroups,function(e,t){return n.opt.filter(t,e,r)},function(e){r++,n.opt.each(e)},function(){0===r&&n.opt.noMatch(e),n.opt.done(r)})}},{key:"mark",value:function(e,t){var n=this;this.opt=t;var r=0,o="wrapMatches",i=this.getSeparatedKeywords("string"==typeof e?[e]:e),a=i.keywords,c=i.length;this.opt.acrossElements&&(o="wrapMatchesAcrossElements"),0===c?this.opt.done(r):function e(t){var i=new s(n.opt).create(t),u=0;n.log('Searching with expression "'.concat(i,'"')),n[o](i,1,function(e,o){return n.opt.filter(o,t,r,u)},function(e){u++,r++,n.opt.each(e)},function(){0===u&&n.opt.noMatch(t),a[c-1]===t?n.opt.done(r):e(a[a.indexOf(t)+1])})}(a[0])}},{key:"markRanges",value:function(e,t){var n=this;this.opt=t;var r=0,o=this.checkRanges(e);o&&o.length?(this.log("Starting to mark with the following ranges: "+JSON.stringify(o)),this.wrapRangeFromIndex(o,function(e,t,r,o){return n.opt.filter(e,t,r,o)},function(e,t){r++,n.opt.each(e,t)},function(){n.opt.done(r)})):this.opt.done(r)}},{key:"unmark",value:function(e){var t=this;this.opt=e;var n=this.opt.element?this.opt.element:"*";n+="[data-markjs]",this.opt.className&&(n+=".".concat(this.opt.className)),this.log('Removal selector "'.concat(n,'"')),this.iterator.forEachNode(NodeFilter.SHOW_ELEMENT,function(e){t.unwrapMatches(e)},function(e){var r=a.matches(e,n),o=t.matchesExclude(e);return!r||o?NodeFilter.FILTER_REJECT:NodeFilter.FILTER_ACCEPT},this.opt.done)}},{key:"opt",set:function(e){this._opt=i({},{element:"",className:"",exclude:[],iframes:!1,iframesTimeout:5e3,separateWordSearch:!0,acrossElements:!1,ignoreGroups:0,each:function(){},noMatch:function(){},filter:function(){return!0},done:function(){},debug:!1,log:window.console},e)},get:function(){return this._opt}},{key:"iterator",get:function(){return new a(this.ctx,this.opt.iframes,this.opt.exclude,this.opt.iframesTimeout)}}]),e}();return e.fn.mark=function(e,t){return new c(this.get()).mark(e,t),this},e.fn.markRegExp=function(e,t){return new c(this.get()).markRegExp(e,t),this},e.fn.markRanges=function(e,t){return new c(this.get()).markRanges(e,t),this},e.fn.unmark=function(e){return new c(this.get()).unmark(e),this},e});

/** 
 *****************************************************************************
 * DataDisplay.js ver 1.0
 *****************************************************************************
 */
var COC = COC || {};
COC.Components.DataDisplayWidget = {
    Init: function(instance) {

    	this.instance = $(instance);
        this.dataURL = this.instance.find('.hidden-vars .csv-data-url').text();
        this.searchType = this.instance.find('.hidden-vars .search-type').text();
        this.searchDropdownOptions = this.instance.find('.hidden-vars .search-dropdown-options').text();        
        this.rowsPerPage = COC.Components.DataDisplayWidget.GetRowsPerPage(this.instance);
        this.initialListState = this.instance.find('.hidden-vars .initial-list-state').text(); 
        
        COC.Components.DataDisplayWidget.Setup(this.instance, this.dataURL);

        if(this.searchType == "Dropdown") {
        	this.BuildDropdown(this.searchDropdownOptions);
        }
        
        if(this.initialListState == "State: Show") {
	        this.GetCSVData(this.dataURL, this.instance);
		}	
    },

    Setup: function(instance, dataURL) {
        // Override default "Enter" submit of input form. This prevents page reload.
        instance.find('#content-search input').keypress(function(e) {
           if (e.keyCode == 13) {
               // Disable default, which causes page reload
               e.preventDefault();
               
               instance.find('.content-search-submit').click();
           }
       });
        
        // clear button
       instance.find('#content-search button.clear-btn').click(function() {
         instance.find('#content-search input').val('');
         instance.find('.content-search-submit').click();
       });


       instance.find('.content-search-submit').click(function() {
           // if(COC.Components.DataDisplayWidget.loaded == false) {
            if(instance.attr("file-loaded") != "true") {
             // Load data
             // Filtering and pagination done in Ajax success method
             COC.Components.DataDisplayWidget.GetCSVData(dataURL, instance);
           } else {
             // Filter list
               var inputValue = $(this).parent().find('.form-control').val();
               COC.Components.DataDisplayWidget.FilterList(instance, inputValue);
               
               // Update pagination
               var paginationTarget = instance.find("#table-contents")[0];
               instance.find("#table-contents").next('.pagination-wrapper').remove();
               COC.PL.PaginationFiltered.AddPagination(paginationTarget, "tbody tr", COC.Components.DataDisplayWidget.GetRowsPerPage(instance));
             }
       });
    },

    GetRowsPerPage: function(instance) {
        return instance.find('.hidden-vars .rows-per-page').text();
    },

    GetCSVData: function(url, instance) {
    	
    	// Show the loading spinner
    	instance.find('.content-lookup-preload-spinner').removeClass('d-none');
    	
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'text',
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            },
            success: function(data) {
                var json = COC.Components.DataDisplayWidget.csvJSON(data);
                var jsonData = JSON.parse(json);

                // Get columns from list
                //var targetColumns = COC.Components.DataDisplayWidget.GetTargetColumns();
                var targetColumns = "*"
                                	
                var paginationTarget = instance.find("#table-contents")[0];
                //COC.Components.DataDisplayWidget.BuildTable(jsonData, targetColumns); // Use zero index, because CSV array is 2D with only first line having data
                COC.Components.DataDisplayWidget.BuildTable(instance, jsonData, targetColumns); // Use zero index, because CSV array is 2D with only first line having data
                COC.PL.PaginationFiltered.AddPagination(paginationTarget, "tbody tr", COC.Components.DataDisplayWidget.GetRowsPerPage(instance));
				
                instance.find('.content-lookup-preload-spinner').addClass('d-none');
                instance.find('.content-lookup-results').removeClass('d-none');
                
                // Filter list
	            var inputValue = instance.find('.dd-search-input').find('.form-control').val();
	            COC.Components.DataDisplayWidget.FilterList(instance, inputValue);
	            
	            // Update pagination
	            instance.find("#table-contents").next('.pagination-wrapper').remove();
	            COC.PL.PaginationFiltered.AddPagination(paginationTarget, "tbody tr", COC.Components.DataDisplayWidget.GetRowsPerPage(instance));
                
                // Prevent page from reloading data on search/filter
                // COC.Components.DataDisplayWidget.loaded = true;
                instance.attr("file-loaded", "true");
                
                // Hide the loading spinner
                instance.find('.content-lookup-preload-spinner').addClass('d-none');
            }
        });
    },

    BuildDropdown: function(optionsString) {
    	var options = optionsString.split("\n");
    	
    	// Create empty option
    	$('<option />', {value: "", text: "Show all"}).appendTo('#select-dropdown');
    	
    	// Create the rest
    	for(var val in options) {
    	    $('<option />', {value: options[val], text: options[val]}).appendTo('#select-dropdown');
    	}    	
    },

//    GetTargetColumns: function() {
//        var keys = [];
//        var targetString = $('.hidden-vars .target-columns').html();
//        var targetColumns = targetString.split("\n");
//
//        if(targetColumns == "") {
//        	targetColumns = "*";
//        }
//        
//        return targetColumns;
//    },

    CSVToArray: function(strData, strDelimiter) {
        // Check to see if the delimiter is defined. If not,
        // then default to comma.
        strDelimiter = (strDelimiter || ",");
        // Create a regular expression to parse the CSV values.
        var objPattern = new RegExp((
            // Delimiters.
            "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
            // Quoted fields.
            "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
            // Standard fields.
            "([^\"\\" + strDelimiter + "\\r\\n]*))"), "gi");
        // Create an array to hold our data. Give the array
        // a default empty first row.
        var arrData = [
            []
        ];
        // Create an array to hold our individual pattern
        // matching groups.
        var arrMatches = null;
        // Keep looping over the regular expression matches
        // until we can no longer find a match.
        while (arrMatches = objPattern.exec(strData)) {
            // Get the delimiter that was found.
            var strMatchedDelimiter = arrMatches[1];
            // Check to see if the given delimiter has a length
            // (is not the start of string) and if it matches
            // field delimiter. If id does not, then we know
            // that this delimiter is a row delimiter.
            if (strMatchedDelimiter.length && (strMatchedDelimiter != strDelimiter)) {
                // Since we have reached a new row of data,
                // add an empty row to our data array.
                arrData.push([]);
            }
            // Now that we have our delimiter out of the way,
            // let's check to see which kind of value we
            // captured (quoted or unquoted).
            if (arrMatches[2]) {
                // We found a quoted value. When we capture
                // this value, unescape any double quotes.
                var strMatchedValue = arrMatches[2].replace(
                    new RegExp("\"\"", "g"), "\"");
            } else {
                // We found a non-quoted value.
                var strMatchedValue = arrMatches[3];
            }
            // Now that we have our value string, let's add
            // it to the data array.
            arrData[arrData.length - 1].push(strMatchedValue);
        }
        // Return the parsed data.
        return (arrData);
    },

    csvJSON: function(csv) {

        //var lines=csv.split("\n");
        var lines = COC.Components.DataDisplayWidget.CSVToArray(csv);

        var result = [];

        // NOTE: If your columns contain commas in their values, you'll need
        // to deal with those before doing the next step 
        // (you might convert them to &&& or something, then covert them back later)
        // jsfiddle showing the issue https://jsfiddle.net/
        var headers = lines[0];

        for (var i = 0; i < lines.length; i++) {

            var obj = {};
            var currentline = lines[i];

            for (var j = 0; j < headers.length; j++) {
                obj[headers[j]] = currentline[j];
            }

            result.push(obj);
        }

        //return result; //JavaScript object
        return JSON.stringify(result); //JSON
    },

    BuildTable: function(instance, data, jKeys) {
        var header = data[0];
        var headerIdx = 0;
        var rowIdx = 0;
        
        var thead = instance.find('.content-lookup').find('#table-contents thead');
        var tbody = instance.find('.content-lookup').find('#table-contents tbody');
        
        // Show all columns and in order
        if (jKeys[0] == "*") {
            for (rowIdx = 0; rowIdx < data.length; rowIdx++) {
                var row = $('<tr data-filter="in" \>');

                for (var key in data[rowIdx]) {
                    if (data[rowIdx].hasOwnProperty(key)) {
                       // console.log(data[rowIdx][key]);
                        if (rowIdx == 0) {
                           // console.log("header: " + data[rowIdx][key]);
                            row.append('<th scope="col">' + data[rowIdx][key] + '</th>');
                            thead.append(row);
                        } else {
                           // console.log("row: " + data[rowIdx][key]);
                            row.append('<td><span class="table-label" aria-hidden="true">' + header[jKeys[headerIdx]] + '</span><span class="table-contents">' + data[rowIdx][key] + '</span></td>');
                            tbody.append(row);
                            headerIdx++;
                        }
                    }
                }
            }
            // Only show specified columns and order
        } else {
            $.each(data, function(index, element) {
                var row = $('<tr data-filter="in" \>');

                for (var i = 0; i < jKeys.length; i++) {
                    if (element[jKeys[i]]) {
                        if (rowIdx == 0) {
                            row.append('<th scope="col">' + element[jKeys[i]] + '</th>');
                            thead.append(row);
                        } else {
                            row.append('<td><span class="table-label" aria-hidden="true">' + header[jKeys[headerIdx]] + '</span><span class="table-contents">' + element[jKeys[i]] + '</span></td>');
                            tbody.append(row);
                            headerIdx++;
                        }
                    }
                }

                headerIdx = 0;
                rowIdx++;
            });
        }

    },

    FilterList: function(instance, input) {
        var strippedInput = input.replace(/[\n\r]+/g, ''); // Strip out new line characters that break the matching. Usually comes from the dropdown.
        var filter = strippedInput.toUpperCase();
        var rows = instance.find('.content-lookup-results #table-contents tbody tr');
        var foundCount = 0;

        instance.find('div.no-results-found').hide();
        
        $(rows).each(function() {
            var found = false;
            var row = this;
            var cells = $(this).find('td');

            //use .map() to get value from each cell as an array
            //then join() with space delimiter. This fixes a bug where search
            //was matching text across table cells
            var rowText = $(this).find('td').find('.table-contents').map(function(){
                				return $(this).text();
              				}).get().join(' '); 
            
            if (rowText.toUpperCase().indexOf(filter) > -1) {
                found = true;
            }

            if (found) {
                $(row).attr('data-filter', 'in').show();
                foundCount++;
            } else {
                $(row).attr('data-filter', 'out').hide();
            }
        });
        
        //clear all highlighting first
        //use 'done' callback to highlight current terms
        instance.find('table#table-contents').unmark({
			'done': function() {
				instance.find('table#table-contents tbody [data-filter="in"]').mark(strippedInput);
			}
		});
        
        if(foundCount == 0) {
        	instance.find('div.no-results-found').show();
        }
    },

//    EditItem: function(itemID) {
//        SP.UI.ModalDialog.showModalDialog({
//            url: '/Lists/PL_DataDisplay/EditForm.aspx?ID=' + itemID,
//            width: 800,
//            height: 475,
//            title: "Edit DataDisplay Item",
//            dialogReturnValueCallback: function(result) {
//                COC.Components.DataDisplayWidget.CloseCallback(result, itemID)
//            }
//        });
//    },
//    CloseCallback: function(result, value) {
//        if (result === SP.UI.DialogResult.OK) {
//            SP.UI.ModalDialog.RefreshPage(result);
//        }
//        if (result === SP.UI.DialogResult.cancel) {}
//    },
};

$(document).ready(function() {	
	var instances = $('div.data-display-widget');
	for(var i=0; i<instances.length; i++) {
		COC.Components.DataDisplayWidget.Init(instances[i])
	}
});
// Example of usage:
/* 
$(document).ready(function(){
	COC.PL.PaginationFiltered.AddPagination("#table-contents", "tbody tr", 5);		// (target, element, elementsPerPage). Elements can be div, li, p, span, whatever 
});

// On filter update, rebuild pagination
$("#table-contents").next('.pagination-wrapper').remove();
COC.PL.PaginationFiltered.AddPagination("#table-contents", "tbody tr", 5);

*/

var COC = COC || {};
COC.PL = COC.PL || {};
COC.PL.PaginationFiltered = (function($) {

	var AddPagination = function(target, element, elementsPerPage) {
	
		if(elementsPerPage <= 0) {
			return;
		}
	
		var elementCount = parseInt($(target).find(element + '[data-filter="in"]').length);
		var pageCount = Math.ceil(elementCount / elementsPerPage);

		//$(target).addClass("pagination row").attr("data-page-num", "1");
		$(target).attr("data-page-num", "1");
		
		// Messes up tables
		//$(target).find(element).wrapAll('<div id="paged-contents" class="col-md-12"/>');

		if (pageCount < 2) {
			return;
		}

		var loadMoreButton = ('<button class="cui btn-lg primary mt-sm mb-md d-none">Load More<span class="cicon-angle-right right" aria-hidden="true"></span></button>');
		var paginationWrapper = $('<div class="pagination-wrapper col-md-12">');
		var 	paginationNav = $('<nav class="pagination-nav" aria-label="Pagination">');
		var 		pageControls = $('<ul class="pagination mobile-basic justify-content-center">');

		if (pageCount > 1) {

			// Normally, pagination would hide/show
			// However, we have to let the external filter handle this, otherwise, it will override the filtered results
			$(target).find(element).hide();

			//   <li class="form-group input-group-sm"><a style="border:none;margin-top:-10px;">Go to page <input size="4" placeholder="page #"></input><button class="btn btn-primary btn-xs>Go</button></a></li>
//			var goToWrapper = $('<li id="go-to-page-wrapper" class="form-group input-group-sm"></li>');
//			var goToText = $('<span>Go to page</span>');
//			var goToInput = $('<input id="go-to-page-num" placeholder="page #" onkeydown="return event.key != \'Enter\';"></input>');					
//			var goToButton = $('<button id="go-to-page-button" class="btn btn-primary btn-xs go-to-page" type="button">Go</button>');					
//			$(goToWrapper).append(goToText);
//			$(goToWrapper).append(goToInput);
//			$(goToWrapper).append(goToButton);
			//COC.PL.PaginationFiltered.AddPaginationClick(target, element, goToButton, elementsPerPage, pageCount);
//			$(goToButton).click(function(){
//				var targetPage = $(this).siblings('#go-to-page-num').val();
//				$(this).closest('.pagination').find(".text-only").remove();
//				$(this).closest('.pagination').find(".page-item").eq(targetPage).click()
//			});
//			$(pageControls).append(goToWrapper);

			var prevButton = $('<li class="page-item prev"><a class="page-link"><span class="text">Prev</span><span class="cui cicon-angle-left right" aria-hidden="true"></span></a> </li>');
			COC.PL.PaginationFiltered.AddPaginationClick(target, element, prevButton, elementsPerPage, pageCount);
			$(pageControls).append(prevButton);

			for (var i = 0; i < pageCount; i++) {
				var pageButton = $('<li class="page-item inactive"><a class="page-link">' + (i + 1) + '</a></li>');		// ".inactive" is counter-intuitive. For this CSS, it's basically "enabled". ".inactive" is enabled.
				COC.PL.PaginationFiltered.AddPaginationClick(target, element, pageButton, elementsPerPage, pageCount);
				$(pageControls).append(pageButton);
			}

			var nextButton = $('<li class="page-item next"><a class="page-link"><span class="text">Next</span><span class="cui cicon-angle-right right" aria-hidden="true"></span></a>  </li>');
			COC.PL.PaginationFiltered.AddPaginationClick(target, element, nextButton, elementsPerPage, pageCount);
			$(pageControls).append(nextButton);
		}

		$(paginationWrapper).append(loadMoreButton);

		$(paginationNav).append(pageControls);
		$(paginationWrapper).append(paginationNav);

		//$(target).append(paginationWrapper);
		$(paginationWrapper).insertAfter(target);
		$(target).addClass('paginated');

		COC.PL.PaginationFiltered.ActivatePagination(target, element, elementsPerPage);
	};

	var AddPaginationClick = function(target, element, button, elementsPerPage, pageCount) {
	    $(button).click(function () {
	        var currentPage = parseInt($(target).attr("data-page-num"));
	        var targetPage = parseInt($(this).text());

	        // Adjust targetPage on Prev or Next click
	        if ($(this).hasClass("prev")) {
	            targetPage = currentPage - 1;
	        } else if ($(this).hasClass("next")) {
	            targetPage = currentPage + 1;
	        } else if ($(this).hasClass("go-to-page")) {
	            targetPage = $("#go-to-page-num").val();
	        }

	        // Prevent wrapping off either side
	        if ((targetPage < 1) || (targetPage > pageCount)) {
	            targetPage = currentPage;
	        }

	        var firstElement = (targetPage - 1) * elementsPerPage;
	        var lastElement = (targetPage * elementsPerPage);

	        //*****************//
	        // Hide all images //
	        //*****************//
	        $(target).find(element).hide();

	        //******************************//
	        // Show only those for the page //
	        //******************************//
	        //$(target).find(element).slice(firstElement, lastElement).show();
	        //$(target).find(element + '[data-filter="in"]').size();
			$(target).find(element + '[data-filter="in"]').slice(firstElement, lastElement).show();

		    // Update data-page-num with targetPage //
		    $(target).attr("data-page-num", targetPage);

	        // Update the buttons
	        COC.PL.PaginationFiltered.UpdatePageButtons(this, targetPage, pageCount);
	        console.log("updated");
	    });
	};

	var ActivatePagination = function(target, element, elementsPerPage) {	
		// Show initial page
		var hasPagination = $(target).hasClass('paginated');
		if (hasPagination) {
			$(target).next('.pagination-wrapper').find('.page-item').eq(1).click(); // Click button 1
		} else {
			$(target).find(target).show()  // Show single images
		}
	};

	var UpdatePageButtons = function(clickedButton, targetPage, pageCount) {
		//$('.pagination').css("display", "none");
		
	    var targetPageIndex = targetPage;

	    // Hide all buttons, then show the ones you need
	    $(clickedButton).parent().find('.page-item').not('#go-to-page-wrapper').hide();

	    // Remove text-only buttons
	    $(clickedButton).siblings('.text-only').remove();

	    // Set all to inactive (box)
//		$(clickedButton).parent().find(".page-item").switchClass("active", "inactive");
	    $(clickedButton).parent().find(".page-item").removeClass("active").addClass("inactive");

		// Update target button state
//		$(clickedButton).parent().find(".page-item").eq(targetPage).switchClass("inactive", "active");
	    $(clickedButton).parent().find(".page-item").eq(targetPage).removeClass("inactive").addClass("active");

	    // Hide/show Prev button
	    if (targetPage == 1) {
	        $(clickedButton).parent().find('.prev').hide();
	    } else {
	        $(clickedButton).parent().find('.prev').show();
	    }
	    // Hide/show Next button
	    if (targetPage == pageCount) {
	        $(clickedButton).parent().find('.next').hide();
	    } else {
	        $(clickedButton).parent().find('.next').show();
	    }

	    // Show the core buttons
	    //
	    // Show first button and last buttons
	    $(clickedButton).parent().find(".page-item").eq(1).show();
	    $(clickedButton).parent().find(".page-item").eq(pageCount).show();

	    // Show buttons before target
	    if (targetPage > 1) {
	        $(clickedButton).parent().find(".page-item").eq(targetPage - 1).show(); // 1 before
	    }
	    if (targetPage == 3) {
	        $(clickedButton).parent().find(".page-item").eq(targetPage - 2).show(); // 2 before
	    }
	    if (targetPage == 4) {
	        $(clickedButton).parent().find(".page-item").eq(targetPage - 1).show(); // 3 before
	        $(clickedButton).parent().find(".page-item").eq(targetPage - 2).show();
	    }

	    // Show target button
	    $(clickedButton).parent().find(".page-item").eq(targetPage).show();

	    // Show button after target
	    if (targetPage < (pageCount - 1)) {
	        $(clickedButton).parent().find(".page-item").eq(targetPage + 1).show(); // 1 after
	    }
	    if (targetPage == (pageCount - 2)) {
	        $(clickedButton).parent().find(".page-item").eq(targetPage + 2).show(); // 2 after
	    }
	    if (targetPage == (pageCount - 3)) {
	        $(clickedButton).parent().find(".page-item").eq(targetPage + 2).show(); // 2 after
	        $(clickedButton).parent().find(".page-item").eq(targetPage + 3).show();
	    }

	    // Add text-only buttons, if needed.
	    if (targetPage > 4) {
	        $(clickedButton).parent().find(".page-item").eq(1).after('<li class="page-item text-only"><span class="page-text">...</span></li>');
	    }
	    if (targetPage < (pageCount - 3)) {
	        $(clickedButton).parent().find(".page-item").eq(pageCount).before('<li class="page-item text-only"><span class="page-text">...</span></li>');
	    }
	    
		//$('.pagination').css("display", "flex");	    
	};

	return {
		AddPagination: AddPagination,
		ActivatePagination: ActivatePagination, 
		AddPaginationClick: AddPaginationClick,
		UpdatePageButtons: UpdatePageButtons
	};
})(jQuery);

