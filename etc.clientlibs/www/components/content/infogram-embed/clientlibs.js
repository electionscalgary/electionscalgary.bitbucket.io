console.log("Infogram JS");
(function($) {

	var COC = COC || {};
	COC.InfogramInit = {
			isScriptAlreadyIncluded: function(src) {
			    var scripts = document.getElementsByTagName("script");
			    for(var i = 0; i < scripts.length; i++) 
			    {
			        if(scripts[i].getAttribute('src') == src)
			        {
			            console.log("Infogram script already loaded"); /*Do not re-load script*/
			            break;

			        } 
			        else
			        {
			         console.log("loading infogram"); /*Fresh start, loaded script*/
			         !function(e,t,n,s){var i="InfogramEmbeds",o=e.getElementsByTagName(t)[0],d=/^http:/.test(e.location)?"http:":"https:";if(/^\/{2}/.test(s)&&(s=d+s),window[i]&&window[i].initialized)window[i].process&&window[i].process();else if(!e.getElementById(n)){var a=e.createElement(t);a.async=1,a.id=n,a.src=s,o.parentNode.insertBefore(a,o)}}(document,"script","infogram-async","https://e.infogram.com/js/dist/embed-loader-min.js");
			         break;

			            }

			    }



		    },
		    WaitforDiv: function()
		    {
		    	
		        if ($( "div" ).hasClass("infogram-embed")) {/*CONTENT: Check if infogram instance was dropped in a page*/
		            COC.InfogramInit.isScriptAlreadyIncluded("https://e.infogram.com/js/dist/embed-loader-min.js"); /*EXTERNAL SCRIPT: Check if infogram script was already created in header.Script to be called just once . Avoid calling script for each isntance */
		    }
		            else
		            {
		            console.log("0 instance"); /*CONTENT: infogram instance was not dropped in a page - do nothing*/

		            }
		    	
		    }


	}


	$(document).ready(function() {
        COC.InfogramInit.WaitforDiv();

	}

    );
	
})(jQuery);
