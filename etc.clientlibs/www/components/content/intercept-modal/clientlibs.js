"use strict";
COC.Components.InterceptModal = function(modalSelector) {
  var
    id = modalSelector.id,
    externalLinks = modalSelector.externalLinks,
    filterId = modalSelector.filterId;
  var filterIdCheck = filterId ? filterId.length > 0 : null;
  var interceptButtonInit = function interceptButtonInit(externalLinks) {
    jQuery.noConflict();

    var modalInit = function modalInit(modalItmLink) {
      jQuery.noConflict();
      window.setTimeout(function() {
        $("#intercept-modal-".concat(id)).modal({
          keyboard: false,
          focus: true,
          backdrop: true
        });

      }, 500);

      // bind
      $("#intercept-modal-".concat(id, " .external-link")).one('click', function(e) {
        window.open(modalItmLink, '_blank');
        $("#intercept-modal-".concat(id)).modal('hide');
      });

      $("#intercept-modal-".concat(id, " .external-link-cancel")).one('click', function(e) {
        $("#intercept-modal-".concat(id)).modal('hide');
      });
    };

    externalLinks.forEach(function(itemLink) {
      var linkElement = $("a[href*='".concat(itemLink, "']"));
      var linkCheck = linkElement ? linkElement.length > 0 : false;
      if (linkCheck) {
        linkElement.each(function(index, itm) {
          var itmLink = itm.href;
          itm.href = '#';
          $(itm).click(function(e) {
            // unbind before to remove previous
            $("#intercept-modal-".concat(id, " .external-link")).off();
            $("#intercept-modal-".concat(id, " .external-link-cancel")).off();
            modalInit(itmLink);
          });
        });
      }
    });
  };

  var modalObserve = function modalObserve() {
    interceptButtonInit(externalLinks);
  };

  // fix for filter dom disappear
  if (window.MutationObserver && filterIdCheck) {
    try {
      var parentSelector = document.querySelector("#".concat(filterId));
      var watchFilterSelection = new MutationObserver(modalObserve);
      watchFilterSelection.observe(parentSelector, {
        childList: !0,
        subtree: !0,
        attributes: !0
      });
    } catch (e) {
      console.log("Intercept modal id is not a valid selector");
    }
  }

  interceptButtonInit(externalLinks);
};