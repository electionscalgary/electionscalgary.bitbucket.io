
(function ($) {
	COC.GETCSS = {
			PET: function(pathtocss,iframeurl,iframeid) {

				//var csssheet = "&stylesheet="+pathtocss;
               var csssheet = "&stylesheet=https://www.calgary.ca/style/COCPets.css"; 
				var myurl = iframeurl+csssheet;
				$('#'+iframeid).attr('src', myurl );


		    },


	},

	
    COC.AutoIframeHeight = {

		startMonitor: function()
		{
			if (window.addEventListener ) {

			    window.addEventListener('message', COC.AutoIframeHeight.handleDocHeightMsg, false);
			    console.log("Done adding EventListener");


			} else if ( window.attachEvent ) { // ie8
			    window.attachEvent('onmessage', COC.AutoIframeHeight.handleDocHeightMsg);
			    console.log("Done attachEvent");


			}	
			
		},
		
        setIframeHeightCO: function (id, ht)
        {
            var ifrm = document.getElementById(id);
            console.log("Targeting "+id);


            ifrm.style.visibility = 'hidden';
            // some IE versions need a bit added or scrollbar appears
            ifrm.style.height = ht + 50 + "px";
            ifrm.style.visibility = 'visible'; 
        },
        handleDocHeightMsg: function(e)
        {
        	var data = JSON.parse( e.data );
       		 // check data object


            if ( data['docHeight'] )
            {
            COC.AutoIframeHeight.setIframeHeightCO( 'wardsform', data['docHeight'] );
       		} 
            else
            {

            COC.AutoIframeHeight.setIframeHeightCO( 'MeetingsFrame', e.data  );

            }


        }

        }


	
	})(jQuery);







(function ($) {
	COC.RefreshContent = {
			StartIntevrals: function() {
				window.setInterval( COC.RefreshContent.ReloadIFrame , 300000);

		    },
		    ReloadIFrame : function()
		    {
		    	var today = new Date().toLocaleString();
		    	var src = $("#myiframe").attr('src');
		    	$("#myiframe").attr('src', src );
		    	$("#guidetime").text("Last update: "+ today);
		    	
		    	
		    }
		    
	}

	
	
	
	$( document ).ready(function() {
		COC.RefreshContent.StartIntevrals();

	});
	})(jQuery);

