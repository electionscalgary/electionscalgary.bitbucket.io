COC = COC || {};
COC.Components = COC.Components || {};

COC.Components.FPEmotiveSearch = function (element)
{
	var _this = this;
	var clickCount = 0;
	
	this.Element = $(element);
	this.SearchBarInput = this.Element.find('.inline-search-bar input');
	this.SearchToggleButton = this.Element.find('.search-toggle-btn');
	this.SearchWindow = this.Element.find(".toggled_search");
	this.SearchSubmit = this.Element.find(".search-submit-btn");
	this.FixedImage = this.Element.find(".fixed-image");
	this.SearchForm = this.Element.find("form");
	this.SearchUrl = "/apps/solr/proxy?corename=suggestions&q=";
	this.searchSuggestions = this.Element.find("div.inner");
	this.typeahead = this.Element.find(".typeahead");
	
	function init()
	{		
		_this.SearchBarInput.focus(openSearchWindow);
		_this.SearchBarInput.blur(closeSearchWindow);	
		_this.SearchWindow.find("button").blur(closeSearchWindow);	
		_this.SearchWindow.blur(closeSearchWindow);	
		_this.SearchToggleButton.click(closeSearchWindow);
		_this.Element.attr("data-state", "closed");
		
		COC.FX.OnRelativeScroll(_this.FixedImage, function(relative) 
		{ 			
			let opacity = ((relative / 6) / 100) + 1;
			_this.FixedImage.css("opacity", opacity); 
		});
		
		setupSearchEvents()
	}
	
	function setupSearchEvents() {
		setupSearchTypeAhead()
		setupSearchSuggestionsClickEvent()
		setupSearchBarKeyDownEvent()
		
		//This is here because iOS isn't registering the 
		//search submit button click target 
		_this.SearchSubmit.on("touchstart", function() {
			_this.SearchForm.submit()
		})
		
		$('.search-submit-icon-only').on("click", function() {
			_this.SearchBarInput.focus()
		})

	}

	function showSuggestions() {
		hideTypeahead()
		_this.searchSuggestions.show()
	}

	function showTypeAhead() {
		if(isTypeaheadVisible()) {
			return
		}
		
		_this.typeahead.attr("data-state", "open")
		_this.searchSuggestions.hide()
		_this.SearchBarInput.autocomplete("search")
	}

	function hideTypeahead() {
		_this.typeahead.attr("data-state", "closed")
	}

	function isTypeaheadVisible() {
		return _this.typeahead.attr("data-state") == "open"
	}

	function setupSearchBarKeyDownEvent() {
		_this.SearchBarInput.keyup(function(e){
			if(e.target.value.trim() == "") {
				showSuggestions()
			}
			else {
				showTypeAhead()
			}
		})
	}

	function openSearchWindow()
	{
		if(_this.SearchBarInput.val() == "") {
			showSuggestions()	
		}
		else {
			showTypeAhead()	
		}
		
		trackSearchBarClick()

		$(document.body).attr("data-mobilemodal", true)
		_this.Element.attr("data-state", "open")
		
		//for iOS. Scroll modal to the top
		var isMobile = window.matchMedia("(max-width: 575px)")
		if(isMobile.matches) {
			setTimeout(function () { window.scrollTo(0, 0); }, 150);
		}		
	}

	function setupSearchSuggestionsClickEvent() {
		_this.SearchWindow.find("button.coc-search-suggestion").on("click", handleSearchSuggestionClick)
		
		//for iOS
		_this.SearchWindow.find("button.coc-search-suggestion").on("touchstart", handleSearchSuggestionClick)
	}
	
	function handleSearchSuggestionClick(e) {
		trackSearchSuggestionClick(e.target.textContent)
		_this.SearchBarInput.val(e.target.textContent)
		_this.SearchForm.submit()
	}
	
	function trackSearchSuggestionClick(suggestionText) {
		ga('send', {
			  hitType: 'event',
			  eventCategory: "Calgary.ca-home-page",
			  eventLabel: suggestionText,
			  eventAction: 'Search-term-click'
			});
	}

	function trackSearchBarClick() {
		ga('send', {
			  hitType: 'event',
			  eventCategory: "Calgary.ca-home-page",
			  eventLabel: ++clickCount,
			  eventAction: 'Search-bar-click'
			});
	}

	function setupSearchTypeAhead() {
        _this.SearchBarInput.autocomplete({
            source: function (request, response) {
                $.ajax({
                	url: _this.SearchUrl + request.term,
                    contentType: "application/json",
                    dataType: "json",
                    timeout: 500,
                    success: function (data) {
                    	
                    	_this.searchSuggestions.hide();
                    	_this.typeahead.show();

                    	var suggestions = [];
                		for(var i=0; i<data.response.docs.length; i++) {
                			suggestions.push(data.response.docs[i].suggestion)
                 		}
                		
                		var dedupedSuggestions = dedupe(suggestions)
                		
                		if(dedupedSuggestions.length < 1) {
                			dedupedSuggestions.push(request.term)
                		}
                		
                      	response($.map(dedupedSuggestions, function (item) {
                            return {
                                label: item,
                                value: item
                            }
                        }));
                    }
                });
            },
            appendTo: _this.typeahead,
            delay: 100,
            minLength: 1,
            select: function(event, ui) {
                if(ui.item){
                	_this.SearchBarInput.val(ui.item.value)
                }

                _this.SearchForm.submit()
            }
        });
	}
	
	function closeSearchWindow(e)
	{
		// if (_this.BlurTimeoutHandle)
		// 	clearTimeout(_this.BlurTimeoutHandle);

		if(!isOpen()) {
			return
		}

		var target = e.relatedTarget;
		//fix for IE11
		if (target == null) { 
			target = document.activeElement;
		}

		if ((target.className.lastIndexOf("coc-search-suggestion") != -1) || 
			(target.className.lastIndexOf("manage-search-suggestions") != -1) ||
			(target.className.lastIndexOf("search-submit-btn") != -1)
			) {
			return
		}

		$(document.body).attr("data-mobilemodal", null);
		//_this.Element.attr("data-state", "closing");
		//setTimeout(function () { _this.Element.attr("data-state", "closed"); }, 250);
		hideTypeahead()
		_this.Element.attr("data-state", "closed");		
	}
		
	 function isOpen()
	 {
	 	return _this.Element.attr("data-state") == "open";
	 }
	
	//TODO: Move to a common area. Also exists in headersearch.js
	//Dedupe query suggestions using Levenshtein algorithm
	function dedupe(suggestions) {
		var dedupedSuggestions = new Array();
		var threshold = 2;
		var distance = 0;
		for (var i = 0; i < suggestions.length; i++) {
			if( i == 0 )
				dedupedSuggestions.push(suggestions[i]);
			else {
				var dupeFlag = false;
				for (var j = 0; j < dedupedSuggestions.length; j++) {
					distance = levDist(suggestions[i], dedupedSuggestions[j]);
					if (distance <= threshold) {
						dupeFlag = true;
						break;
					}
				}
				if (dupeFlag == false) 
					dedupedSuggestions.push(suggestions[i]);
			}
		}
		return dedupedSuggestions;
	}
	
	//TODO: Move to a common area. Also exists in headersearch.js
	//Levenshtein algorithm to calculate edit distance between two strings. Needed to deduplicate near matches in query suggestions.
	function levDist(s, t) {
		var d = []; //2d matrix
	
		// Step 1
		var n = s.length;
		var m = t.length;
	
		if (n == 0) return m;
		if (m == 0) return n;
	
		//Create an array of arrays in javascript (a descending loop is quicker)
		for (var i = n; i >= 0; i--) d[i] = [];
	
		// Step 2
		for (var i = n; i >= 0; i--) d[i][0] = i;
		for (var j = m; j >= 0; j--) d[0][j] = j;
	
		// Step 3
		for (var i = 1; i <= n; i++) {
			var s_i = s.charAt(i - 1);
	
			// Step 4
			for (var j = 1; j <= m; j++) {
	
				//Check the jagged ld total so far
				if (i == j && d[i][j] > 4) return n;
	
				var t_j = t.charAt(j - 1);
				var cost = (s_i == t_j) ? 0 : 1; // Step 5
	
				//Calculate the minimum
				var mi = d[i - 1][j] + 1;
				var b = d[i][j - 1] + 1;
				var c = d[i - 1][j - 1] + cost;
	
				if (b < mi) mi = b;
				if (c < mi) mi = c;
	
				d[i][j] = mi; // Step 6
	
				//Damerau transposition
				if (i > 1 && j > 1 && s_i == t.charAt(j - 2) && s.charAt(i - 2) == t_j) {
					d[i][j] = Math.min(d[i][j], d[i - 2][j - 2] + cost);
				}
			}
		}
	
		// Step 7
		return d[n][m];
	}
	
	init();
};
