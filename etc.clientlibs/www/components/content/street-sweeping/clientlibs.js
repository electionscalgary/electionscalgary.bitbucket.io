"use strict";

/*
 * @Date:   2020-12-15 14:01:32
 * @Last Modified time: 2021-02-09 16:26:09
 */

COC.AddressLookup = COC.AddressLookup || {};

COC.AddressLookup.checkFreeFormAndSelected = /*#__PURE__*/ function() {
  var _ref = _asyncToGenerator( /*#__PURE__*/ regeneratorRuntime.mark(function _callee(checkFreeFormAndSelectedParams) {
    var validatedAddress, inputValidationConfig, closestAddresslookupID, addressLookupFunctions, addressValidated, theoretical, chosenReturnedData;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:

            validatedAddress =


              checkFreeFormAndSelectedParams.validatedAddress, inputValidationConfig = checkFreeFormAndSelectedParams.inputValidationConfig, closestAddresslookupID = checkFreeFormAndSelectedParams.closestAddresslookupID;
            addressLookupFunctions = COC.Components.AddressLookup(closestAddresslookupID);
            addressValidated = COC.Input.inputValidation(inputValidationConfig);

            // did we get a theoretical back?
            theoretical = {};
            if (!


              addressValidated.validated) {
              _context.next = 7;
              break;
            }
            _context.next = 11;
            break;
          case 7:
            _context.next = 9;
            return (



              addressLookupFunctions.setupAddressEvents().chosenTriggered());
          case 9:
            chosenReturnedData = _context.sent;
            // take the top match / perfect fit
            theoretical = {
              validatedAddress: validatedAddress,
              theoreticalDataValues: chosenReturnedData.candidates[0]
            };
          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return function(_x) {
    return _ref.apply(this, arguments);
  };
}();




COC.AddressLookup.checkTheoreticalDone = function(theoreticalParams) {
  var
    validatedAddress = theoreticalParams.validatedAddress,
    closestAddresslookupID = theoreticalParams.closestAddresslookupID;
  if (validatedAddress) {
    var theoreticalDataValues = COC.Components.AddressLookup(closestAddresslookupID).getSetFoundAddressData('theoreticalAddress').get();
    if (theoreticalDataValues.address) {
      var returnFoundTheoreticalChunk = {
        validatedAddress: validatedAddress,
        theoreticalDataValues: theoreticalDataValues
      };

      return returnFoundTheoreticalChunk;
    } else {
      // no theoretical values
    }
  }
};

COC.AddressLookup.addResultsToPage = function(params) {
  var
    resultsContainer = params.resultsContainer,
    htmlToAppend = params.htmlToAppend;
  if ($(resultsContainer).length > 0) {
    $(resultsContainer).html('');
    $(resultsContainer).html(htmlToAppend);
    $(resultsContainer).removeClass('d-none');
  }
};

// eslint-disable-next-line no-undef
COC.AddressLookup.fromLatLong = function(lookUpValue) {
  return new Promise(function(resolve, reject) {
    // gets an address from a lat long
    var gisEndPoint = "".concat(window.location.protocol, "//gis.calgary.ca/arcgis/rest/services/pub_Locators/ReverseCompositeLocatorLL/GeocodeServer/reverseGeocode?location=").concat(lookUpValue, "&distance=50&langCode=&outSR=&returnIntersection=false&f=json");

    var cleanUpData = function cleanUpData(data) {
      var addressCleanUp;
      if (data.address !== undefined) {
        // clean up the data
        addressCleanUp = data.address.Address;
        if (addressCleanUp.indexOf(';') > 0) {
          // take the second one, as the first is often a place name
          addressCleanUp = addressCleanUp.split(';')[1];
        }
      }
      return addressCleanUp;
    };

    // get the candidates from the GIS endpoint
    $.ajax({
      url: gisEndPoint,
      type: 'GET',
      dataType: 'json',
      success: function success(data, status) {
        var cleanedUpData = cleanUpData(data);
        resolve(cleanedUpData);
        resolve(data);
      },
      error: function error(request, errorType, errorMessage) {
        console.log('address look up gis data failed');
        reject();
      }
    });

  });
};
"use strict";
/*
 * @Date:   2020-12-15 14:01:32
 * @Last Modified time: 2021-02-09 16:34:22
 */

COC.Components = COC.Components || {};

COC.Components.AddressLookup = function() {
  var parentID = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  // let addressLookupParams = addressLookupParams || {}; 
  var addressLookupParams = {};
  // let inputValidationConfig = inputValidationConfig || {};
  var inputValidationConfig = {};

  var foundAddresses = {};

  var setupAddressEvents = function setupAddressEvents() {
    var chosenTriggered = function chosenTriggered(callback) {
      return new Promise(function(resolve, reject) {
        var typedValue = addressLookupParams.inputEl.val().trim();
        var chosenAddress = {
          value: typedValue,
          inputSource: 'typed'
        };
        onAddressChosen(chosenAddress).then(function(data) {
          resolve(data);
          if (callback) {
            callback();
          }
        })["catch"](
          function(err) {
            console.log(err);
            reject(err);
          });
        return chosenAddress;
      });
    };

    var chosenEnterInit = function chosenEnterInit() {
      addressLookupParams.inputEl.on('change keydown paste input', function(event) {
        if (event.keyCode == 13) {
          chosenTriggered();
          // returning false prevents beeping in IE browser
          return false;
        }
      });
    };

    return {
      chosenTriggered: chosenTriggered,
      chosenEnterInit: chosenEnterInit
    };

  };

  function filterIfPerfectMatch(fetchSuggestionsParams, suggestionsReturned) {
    var perfectMatch = suggestionsReturned.candidates.filter(function(candidates) {
      return candidates.address === fetchSuggestionsParams.normalizedAddressInput;
    });
    var returnedMatch;
    if (perfectMatch.length > 0) {
      suggestionsReturned.perfectMatchFound = true;
      suggestionsReturned.candidates = perfectMatch;
      var test = perfectMatch[0];
    } else {
      suggestionsReturned.perfectMatchFound = false;
      if (suggestionsReturned.candidates.length > 0) {
        var sortedDataCandidates = COC.Utils.sortArray(suggestionsReturned.candidates, 'score', 'desc');
        suggestionsReturned.candidates = sortedDataCandidates;
      }
    }
    return suggestionsReturned;
  }

  function reverseNormalize(reversingAddress) {
    var normalReverse = COC.Components.NormalizeAddresses(reversingAddress, {
      reverse: true
    });
    return normalReverse;
  }

  function fetchSuggestions(fetchSuggestionsParams) {
    return new Promise(function(resolve, reject) {
      var

        normalizedAddressInput =



        fetchSuggestionsParams.normalizedAddressInput,
        request = fetchSuggestionsParams.request,
        response = fetchSuggestionsParams.response,
        source = fetchSuggestionsParams.source;

      var gisEndPoint = "https://gis.calgary.ca/arcgis/rest/services/pub_Locators/CalgaryRSAddressLocator/GeocodeServer/findAddressCandidates?Address=".concat(normalizedAddressInput, "&Neighborhood=&City=&Subregion=&State=&Country=&SingleLine=&category=&outFields=X%2CY%2C&maxLocations=&outSR=4326&maxLocations=&outSR=&searchExtent=&location=&distance=&magicKey=&f=pjson");
      $.getJSON(gisEndPoint, function(suggestionsData) {
        return suggestionsData;
      }).
      done(function(suggestionsData) {
        if (response) {
          // foundCandidates = {};
          if (suggestionsData.candidates.length > 0) {
            // const foundCandidates = extractCandidates(suggestionsData.candidates);
            response($.map(suggestionsData.candidates, function(suggestionReturnedData) {
              var normalizedAddressOutput = reverseNormalize(suggestionReturnedData.address);

              // attached  suggestionReturnedData, so we don't have to re-look it up when the address is chosen
              var suggestionData = {
                label: normalizedAddressOutput,
                value: normalizedAddressOutput,
                suggestionReturnedData: suggestionReturnedData
              };

              return suggestionData;
            }));
          }
        } else {
          if (source == 'freeFormTyped') {
            // const perfectMatch = suggestionsData.candidates.filter((candidates) => candidates.address === fetchSuggestionsParams.normalizedAddressInput);
            // if (perfectMatch.length > 0) {
            //   suggestionsData.perfectMatchFound = true;
            //   suggestionsData.candidates = perfectMatch;
            //   resolve(suggestionsData);
            // } else {
            //   suggestionsData.perfectMatchFound = false;
            //   if (suggestionsData.candidates.length > 0) {
            //     const sortedDataCandidates = COC.Utils.sortArray(suggestionsData.candidates, 'score', 'desc');
            //     suggestionsData.candidates = sortedDataCandidates;
            //   }
            //   resolve(suggestionsData);
            // }

            var perfectCleanedResult = filterIfPerfectMatch(fetchSuggestionsParams, suggestionsData);
            resolve(perfectCleanedResult);
          } else {
            // not free form
          }
        }
      }).
      fail(function() {
        console.log('fetchSuggestions error');
        reject();
      });
    });
  }

  var setupAddressAutoCompleteSource = function setupAddressAutoCompleteSource(request, response) {
    var inputtedAddress = addressLookupParams.inputEl.val().trim();
    // fix inputted address so machine can read it
    var normalizedAddressInput = COC.Components.NormalizeAddresses(inputtedAddress);
    // If the first word is a number, wait till the person starts typing the street before we send a request for suggestions
    var delayInput = inputtedAddress.split(' ');
    var delayInputFirstWord = parseFloat(delayInput[0]);

    // check to see if the validation has changed
    var setStreetData = getSetFoundAddressData('streetAddress').get();
    if (setStreetData.suggestedAddress) {
      setStreetData = setStreetData.suggestedAddress.streetAddress;
    }

    // fetch new suggested addresses if the input does not match any stored address data
    if (inputtedAddress !== setStreetData) {
      // set the address to be invalid
      inputValidationConfig = {
        selectorParent: addressLookupParams.parent,
        selector: addressLookupParams.inputEl,
        type: '',
        message: ''
      };


      removeValidation(inputValidationConfig);

      // if the first "word" is number, wait till the street name is being typed before requesting a suggestion
      // it would be useful to make this a better experience by having it ignore the number and provide better street suggestions
      if (typeof delayInputFirstWord === 'number') {
        if (delayInput[1]) {
          if (delayInput[1].length >= 1) {
            var fetchSuggestionsParams = {
              normalizedAddressInput: normalizedAddressInput,
              request: request,
              response: response
            };

            fetchSuggestions(fetchSuggestionsParams);
          }
        } else {
          console.log('not enough characters');
        }
      } else {
        console.log('not enough characters');
      }
    }
  };

  function setupAddressAutoComplete() {
    addressLookupParams.inputEl.autocomplete({
      appendTo: addressLookupParams.parent,
      source: setupAddressAutoCompleteSource,
      delay: 300,
      minLength: 4,
      select: function select(event, ui) {
        var gisStreetData = ui.item.suggestionReturnedData;
        var chosenAddress = {
          streetAddress: ui.item.value,
          inputSource: 'selected',
          gisStreetData: gisStreetData
        };
        onAddressChosen(chosenAddress);
      }
    });

  }

  var fetchTheorectical = function fetchTheorectical(chosenAddressValue) {
    var gisTheoreticalEndPoint = "https://gis.calgary.ca/arcgis/rest/services/pub_Locators/CalgaryTheoreticalLocator/GeocodeServer/findAddressCandidates?Street=".concat(chosenAddressValue, "&Single+Line+Input=&category=&outFields=X%2CY%2C&maxLocations=&outSR=4326&=&searchExtent=&location=&distance=&magicKey=&f=pjson");
    return new Promise(function(resolve, reject) {
      $.getJSON(gisTheoreticalEndPoint, function(getJSONData) {
        return getJSONData;
      }).
      done(function(theoreticalData) {
        var foundTheoretical = [];
        var perfectMatch = theoreticalData.candidates.filter(function(candidates) {
          return candidates.address === chosenAddressValue;
        });
        if (perfectMatch.length > 0) {
          foundTheoretical = theoreticalData.candidates[0];
        } else {
          if (theoreticalData.candidates.length == 1) {
            foundTheoretical = theoreticalData.candidates[0];
          } else {
            foundTheoretical = theoreticalData.candidates;
            var sortedDataCandidates = COC.Utils.sortArray(foundTheoretical, 'score', 'desc');
            foundTheoretical = sortedDataCandidates[0];
          }
        }
        resolve(foundTheoretical);
      }).
      fail(function(error) {
        console.log('error');
        console.log('error: ' + error);
        reject();
      });
    });
  };

  function getSetFoundAddressData(dataType) {
    var domElement;
    var getDomElement = function getDomElement(dataType) {
      switch (dataType) {
        case 'streetAddress': {
          domElement = $("".concat(addressLookupParams.parent, " ").concat(addressLookupParams.streetEl));
          break;
        }
        case 'theoreticalAddress': {
          domElement = $("".concat(addressLookupParams.parent, " ").concat(addressLookupParams.theoreticalEl));
          break;
        }
        default: {}
      }

      return domElement;
    };
    getDomElement(dataType);

    return {
      set: function set(chosenAddress) {
        chosenAddress.source = dataType;
        if (domElement) {
          domElement.text(JSON.stringify(chosenAddress));
        }
      },
      get: function get() {
        if (domElement) {
          var streetData = domElement.text();
          if (streetData.length > 0) {
            streetData = JSON.parse(domElement.text());
          }
          return streetData;
        }
      },
      clear: function clear() {
        var clearAll = ['streetAddress', 'theoreticalAddress'];
        if (dataType) {
          clearAll = [dataType];
        }
        clearAll.forEach(function(item) {
          var domToClear = getDomElement(item);
          domToClear.text('');
        });
      }
    };

  }

  function noAddressFound() {
    removeValidation(inputValidationConfig);
    validation('invalid');
    inputValidationConfig.selector.focus();
  }

  function addressIsValid(validationParams) {
    return new Promise(function(resolve, reject) {
      var

        validatedChosen =

        validationParams.validatedChosen,
        chosenAddress = validationParams.chosenAddress;

      if (validatedChosen === true) {
        // set as valid
        validation('valid');
        var setStreetData = getSetFoundAddressData('streetAddress');
        setStreetData.set(chosenAddress);
        var theoretical = fetchTheorectical(chosenAddress.gisStreetData.address);
        theoretical.then(function(theoreticalData) {
          var setTheoreticalData = getSetFoundAddressData('theoreticalAddress');
          setTheoreticalData.set(theoreticalData);
          resolve(theoreticalData);
        });
      }
      // reject();
    });
  }

  function similarAddressMessage(validationParams) {
    var
      address = validationParams.chosenAddress.gisStreetData.address;
    var message = "<p class=\"mb-xs d-inline\">We found an address that is similar, would you like to use ".concat(address, " instead?</p>\n    <span class=\"d-block\">\n      <button class=\"cui btn-md utility-btn-solid p-m similar-address-yes\" href=\"#\">Yes</button>\n      <button class=\"cui btn-md utility-btn-solid p-m similar-address-no\" href=\"#\">No</button>\n     </span>");





    inputValidationConfig.type = 'warning';
    inputValidationConfig.message = message;

    COC.Input.inputValidation(inputValidationConfig);

    $('.similar-address-yes').click(function() {
      var inputBoxEl = $('.address-lookup-inputbox');
      inputBoxEl.val(address);
      inputValidationConfig.type = 'remove';
      inputValidationConfig.message = '';
      COC.Input.inputValidation(inputValidationConfig);

      validationParams.chosenAddress.value = address;
      validationParams.validatedChosen = true;
      addressIsValid(validationParams);
    });

    $('.similar-address-no').click(function() {
      noAddressFound();
    });
  }

  function onAddressChosen(chosenAddress) {
    return new Promise(function(resolve, reject) {
      var validationParams = {
        validatedChosen: false
      };

      if (chosenAddress.inputSource === 'selected') {
        // if the address was selected from the list it is a valid address as the source of the address is the GIS system
        validationParams.validatedChosen = true;
        validationParams.chosenAddress = chosenAddress;
        addressIsValid(validationParams);
      } else if (chosenAddress.inputSource === 'typed') {
        var typedInAddress = chosenAddress.value.toUpperCase();
        // check to see if we have already retrieved the address as the single top suggestion
        if (foundAddresses.length === 1) {
          var normalizedfoundAddress = COC.Components.NormalizeAddresses(foundAddresses[0], {
            reverse: true
          });
          if (typedInAddress === normalizedfoundAddress || typedInAddress === foundAddresses[0]) {
            validationParams.validatedChosen = true;
          }
        } else {
          chosenAddress.inputSource = 'freeFormTyped';
          var typedNormalizedAddress = COC.Components.NormalizeAddresses(typedInAddress);

          var fetchSuggestionsParams = {
            normalizedAddressInput: typedNormalizedAddress,
            source: chosenAddress.inputSource
          };


          var freeFormAddress = fetchSuggestions(fetchSuggestionsParams);

          // make this a promise,  when using fetchSuggestions to only populate the to dropdown, the promise is not needed
          freeFormAddress.then(function(suggestionsData) {
            chosenAddress.gisStreetData = suggestionsData.candidates[0];
            // chosenAddress.streetAddress=
            validationParams.chosenAddress = chosenAddress;
            if (suggestionsData.perfectMatchFound) {
              if (suggestionsData.candidates[0].score > 0) {
                validationParams.validatedChosen = true;
                addressIsValid(validationParams).then(function(theoreticalData) {
                  suggestionsData.source = chosenAddress.inputSource;
                  var addressesData = {
                    theoreticalData: theoreticalData,
                    streetData: suggestionsData
                  };
                  resolve(addressesData);
                })["catch"](function(err) {
                  console.log(err);
                });
              } else {
                // not valid
              }
            } else {
              validationParams.validatedChosen = false;
              if (chosenAddress.gisStreetData) {
                similarAddressMessage(validationParams);
              } else {
                noAddressFound();
                resolve('no suggestions found');
              }
            }
          })["catch"](
            function(err) {
              console.log("free form address not found");
              console.log(err);
            });
        }
      } else {
        console.log('not selected or typed');
      }
    });
  }

  function validation(type) {
    // for a reset of values
    inputValidationConfig.type = type;
    COC.Input.inputValidation(inputValidationConfig);
  }

  function removeValidation(inputValidationConfig) {
    // for a reset of values
    inputValidationConfig.type = 'remove';
    inputValidationConfig.message = '';
    COC.Input.inputValidation(inputValidationConfig).hide();
  }

  function init() {
    var initParentID = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    addressLookupParams = {
      parentWrapper: '.address-lookup-wrapper',
      parent: initParentID,
      inputEl: $("".concat(initParentID, " .address-lookup-inputbox")),
      dataContainer: '.address-lookup-data-container',
      resultsContainer: '.address-lookup-results-container',
      streetEl: '.address-lookup-data-street',
      theoreticalEl: '.address-lookup-data-theoretical',
      setupAddressEvents: setupAddressEvents()
    };


    inputValidationConfig = {
      selectorParent: addressLookupParams.parent,
      selector: addressLookupParams.inputEl,
      type: '',
      message: ''
    };


    addressLookupParams.setupAddressEvents.chosenEnterInit();
    setupAddressAutoComplete();

    return {
      addressLookupParams: addressLookupParams,
      inputValidationConfig: inputValidationConfig
    };

  }

  var initParams = init(parentID);

  return {
    setupAddressEvents: setupAddressEvents,
    fetchTheorectical: fetchTheorectical,
    removeValidation: removeValidation,
    getSetFoundAddressData: getSetFoundAddressData,
    initParams: initParams
  };

};
"use strict";
/*
 * @Date:   2020-12-15 14:01:32
 * @Last Modified time: 2021-01-11 15:43:34
 */

COC.Components = COC.Components || {};
COC.Components.NormalizeAddresses = function(address) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var _options$reverse =
    options.reverse,
    reverse = _options$reverse === void 0 ? false : _options$reverse;

  var quadrants = [{
      name: 'NORTHWEST',
      code: 'NW'
    },

    {
      name: 'SOUTHWEST',
      code: 'SW'
    },

    {
      name: 'NORTHEAST',
      code: 'NE'
    },

    {
      name: 'SOUTHEAST',
      code: 'SE'
    }
  ];



  var streetTypes = [{
      names: ['ALLEY', 'ALLY', 'ALY'],
      name: ['ALLEY'],
      code: 'AL'
    },

    {
      names: ['AVENUE', 'AVE'],
      name: ['AVENUE'],
      code: 'AV'
    },

    {
      names: ['BAY'],
      name: ['BAY'],
      code: 'BA'
    },

    {
      names: ['BOULEVARD', 'BLVD'],
      name: ['BOULEVARD'],
      code: 'BV'
    },

    {
      names: ['CAPE'],
      name: ['CAPE'],
      code: 'CA'
    },

    {
      names: ['CENTRE', 'CTR'],
      name: ['CENTRE'],
      code: 'CE'
    },

    {
      names: ['CIRCLE', 'CIRC', 'CIR'],
      name: ['CIRCLE'],
      code: 'CI'
    },

    {
      names: ['CLOSE'],
      name: ['CLOSE'],
      code: 'CL'
    },

    {
      names: ['COMMON'],
      name: ['COMMON'],
      code: 'CM'
    },

    {
      names: ['COURT', 'CT'],
      name: ['COURT'],
      code: 'CO'
    },

    {
      names: ['COVE'],
      name: ['COVE'],
      code: 'CV'
    },

    {
      names: ['CRESCENT', 'CRES'],
      name: ['CRESCENT'],
      code: 'CR'
    },

    {
      names: ['DRIVE'],
      name: ['DRIVE'],
      code: 'DR'
    },

    {
      names: ['GARDENS', 'GRDNS', 'GDNS'],
      name: ['GARDENS'],
      code: 'GD'
    },

    {
      names: ['GATE'],
      name: ['GATE'],
      code: 'GA'
    },

    {
      names: ['GREEN', 'GRN'],
      name: ['GREEN'],
      code: 'GR'
    },

    {
      names: ['GROVE', 'GRV'],
      name: ['GROVE'],
      code: 'GV'
    },

    {
      names: ['HEATH'],
      name: ['HEATH'],
      code: 'HE'
    },

    {
      names: ['HEIGHTS', 'HTS'],
      name: ['HEIGHTS'],
      code: 'HT'
    },

    {
      names: ['HIGHWAY'],
      name: ['HIGHWAY'],
      code: 'HI'
    },

    {
      names: ['HILL'],
      name: ['HILL'],
      code: 'HL'
    },

    {
      names: ['ISLAND'],
      name: ['ISLAND'],
      code: 'IS'
    },

    {
      names: ['LANDING', 'LNDG', 'LDG'],
      name: ['LANDING'],
      code: 'LD'
    },

    {
      names: ['LANE'],
      name: ['LANE'],
      code: 'LN'
    },

    {
      names: ['LINK'],
      name: ['LINK'],
      code: 'LI'
    },

    {
      names: ['MANOR', 'MNR'],
      name: ['MANOR'],
      code: 'MR'
    },

    {
      names: ['MEWS'],
      name: ['MEWS'],
      code: 'ME'
    },

    {
      names: ['MOUNT', 'MNT'],
      name: ['MOUNT'],
      code: 'MT'
    },

    {
      names: ['ND'],
      name: ['ND'],
      code: ''
    },

    {
      names: ['PARADE'],
      name: ['PARADE'],
      code: 'PR'
    },

    {
      names: ['PARK', 'PRK', 'PK'],
      name: ['PARK'],
      code: 'PA'
    },

    {
      names: ['PARKWAY', 'PKWY'],
      name: ['PARKWAY'],
      code: 'PY'
    },

    {
      names: ['PASSAGE', 'PASS'],
      name: ['PASSAGE'],
      code: 'PS'
    },

    {
      names: ['PATH'],
      name: ['PATH'],
      code: 'PH'
    },

    {
      names: ['PLACE'],
      name: ['PLACE'],
      code: 'PL'
    },

    {
      names: ['PLAZA', 'PLZ'],
      name: ['PLAZA'],
      code: 'PZ'
    },

    {
      names: ['POINT'],
      name: ['POINT'],
      code: 'PT'
    },

    {
      names: ['RISE'],
      name: ['RISE'],
      code: 'RI'
    },

    {
      names: ['ROAD'],
      name: ['ROAD'],
      code: 'RD'
    },

    {
      names: ['ROW'],
      name: ['ROW'],
      code: 'RO'
    },

    {
      names: ['SQUARE', 'SQU'],
      name: ['SQUARE'],
      code: 'SQ'
    },

    {
      names: ['STREET', 'ST'],
      name: ['STREET'],
      code: 'ST'
    },

    {
      names: ['TERRACE', 'TERR', 'TER'],
      name: ['TERRACE'],
      code: 'TC'
    },

    {
      names: ['TH'],
      name: ['TH'],
      code: ''
    },

    {
      names: ['TRAIL', 'TRA'],
      name: ['TRAIL'],
      code: 'TR'
    },

    {
      names: ['VILLAS', 'VILL'],
      name: ['VILLAS'],
      code: 'VI'
    },

    {
      names: ['VIEW'],
      name: ['VIEW'],
      code: 'VW'
    },

    {
      names: ['WALK', 'WLK'],
      name: ['WALK'],
      code: 'WK'
    },

    {
      names: ['WAY'],
      name: ['WAY'],
      code: 'WY'
    }
  ];



  var addressToReturn = '';
  // trim the address
  var trimAddress = address.replace('.', '').toUpperCase().trim();

  // filter out the word Calgary
  var replaceCalgary = trimAddress.replace('CALGARY', '').trim();

  // remove any street number suffixes / ordinals
  var stripAddress = replaceCalgary.replace(/(\d+)(?:ST|ND|RD|TH)/, '$1');

  var toTitleCase = function toTitleCase(item) {
    var titledCase = '';
    if (item.length > 1) {
      titledCase = item.split(' ').
      map(function(word) {
        return word[0].toUpperCase() + word.substr(1).toLowerCase();
      }).
      join(' ');
    } else {
      titledCase = item;
    }
    return titledCase;
  };

  // fuzzy logic to convert the persons input string to the machine readable street
  // caveat is that some of the street names look like street types, e.g. Crescent Road NW
  // so search from the back of the string, and only match the first or last word
  var matchToStreetType = '';
  var addressSplit;

  var matchStreetTypes = function matchStreetTypes(matchStreetType, reversed) {
    var matchToStreetType;
    streetTypes.map(function(streetType) {
      // check to see if the names match the inputted string
      // if the street type is reversed it is a machine to human conversion
      if (reversed) {
        if (streetType.code === matchStreetType) {
          matchToStreetType = matchStreetType.replace(matchStreetType, streetType.name);
        }
      } else {
        if (streetType.names.includes(matchStreetType)) {
          matchToStreetType = matchStreetType.replace(matchStreetType, streetType.code);
        } else if (streetType.code === matchStreetType) {
          matchToStreetType = matchStreetType;
        }
      }
    });
    return matchToStreetType;
  };

  if (reverse == false) {
    // translate to machine
    var nonReversedSplit = stripAddress.split(' ').reverse();
    // check to see if the last word is a quadrant, and match the street type to the second last word
    // if not, match the street type to the last word
    // nonReversedSplit = nonReversedSplit.replace('.', '');
    if (nonReversedSplit[0].match(/^(NORTHWEST|SOUTHWEST|NORTHEAST|SOUTHEAST|NW|SW|NE|SE)$/)) {
      // fix any quadrants
      quadrants.map(function(quadrant) {
        if (nonReversedSplit[0] === quadrant.name) {
          nonReversedSplit[0] = nonReversedSplit[0].replace(quadrant.name, quadrant.code);
        } else {}
      });
      matchToStreetType = nonReversedSplit[1];
      nonReversedSplit[1] = matchStreetTypes(matchToStreetType);
    } else {
      var checkIfNumber = isFinite(nonReversedSplit[0]);
      matchToStreetType = nonReversedSplit[0];
      var streetTypeMatched = matchStreetTypes(matchToStreetType);
      if (streetTypeMatched) {
        nonReversedSplit[0] = streetTypeMatched;
      }
    }

    addressToReturn = nonReversedSplit.reverse().join(' ');
  } else {
    // translate from machine
    var reversedSplit = replaceCalgary.split(' ').reverse();
    // check to see if the last word is a quadrant, and match the street type to the second last word
    // if not, match the street type to the last word
    if (reversedSplit[0].match(/^(NORTHWEST|SOUTHWEST|NORTHEAST|SOUTHEAST|NW|SW|NE|SE)$/)) {
      // fix any quadrants
      // quadrants.map(quadrant => {
      //   if (reversedSplit[0] === quadrant.code) {
      //     reversedSplit[0] = reversedSplit[0].replace(quadrant.code, quadrant.name);
      //   } else {}
      // });

      matchToStreetType = reversedSplit[1];
      reversedSplit[1] = matchStreetTypes(matchToStreetType, reverse);
    } else {
      matchToStreetType = reversedSplit[0];
      reversedSplit[0] = matchStreetTypes(matchToStreetType, reverse);
      reversedSplit[0] = reversedSplit[0].toLowerCase();
    }

    var reversedSplitTitled = reversedSplit.map(function(addressPart, index) {
      return index == 0 ? addressPart : toTitleCase(addressPart);
    });
    addressToReturn = reversedSplitTitled.reverse().join(' ');
  }

  return addressToReturn;
};
"use strict";
COC.Component = COC.Component || {};
COC.Component.AddressLookupAnalytics = function() {
  $(function() {
    $('.address-lookup-wrapper .address-lookup-submit-button').click(function(e) {
      e.preventDefault();

      var title = $(this).text().trim().replace(/[^a-zA-Z 0-9]+/g, '');
      var buttonName = title.toLowerCase().replace(/\s/g, '-');

      var gaCategory = 'Address lookup';
      var eventAction = "Address-lookup-".concat(buttonName, "-button");
      var eventLabel = "Address-lookup-".concat(buttonName, "-button");

      ga('send', 'event', gaCategory, eventAction, eventLabel);
    });
  });
}();
"use strict";

COC.Components = COC.Components || {};
COC.Components.StreetSweeping = function() {
  var parentID = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var streetSweepingParams = {
    parentID: parentID,
    submitButton: $('#street-sweeping-submit'),
    resultsWrapper: '#street-sweeping-results-wrapper'
  };


  var gisServerParams = {
    environment: 'Prod',
    mapServerURL: 'https://gis.calgary.ca/arcgis/rest/services/pub_CalgaryDotCa/SweepRoutes/MapServer',
    postOutFields: [{
        scheduled_date: 'ROADSNOTIFY.SCHEDULE.SCHEDULED_DT'
      },

      {
        sweep_type: 'ROADSNOTIFY.SCHEDULE.DAY_NIGHT'
      },
      {
        duration: 'ROADSNOTIFY.SCHEDULE.DURATION'
      },
      {
        routeStreetName: 'RM_SCU_ROUTE.FULL_NAME'
      },
      {
        PARK_BAN_YN: 'RM_SCU_ROUTE.PARK_BAN_YN'
      }
    ],

    mapLayerNames: [{
      name: 'Schedules',
      id: 0
    }],
    mapURL: 'https://maps.calgary.ca/StreetSweeping/?find=',
    nestFeatureOn: 'PARKING_BAN_TYPE'
  };

  function


  getAllStreetSweepingData() {
    return _getAllStreetSweepingData.apply(this, arguments);
  }

  function _getAllStreetSweepingData() {
    _getAllStreetSweepingData = _asyncToGenerator( /*#__PURE__*/ regeneratorRuntime.mark(function _callee() {
      var checkFreeFormAndSelectedParams, addressValidated, theoretical, chosenReturnedData, validatedAddress, theoreticalParams, _yield$streetSweeping, theoreticalData, getLayersParams, getLayerData;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              // check to see if the address has been returned and validated
              checkFreeFormAndSelectedParams = {
                inputValidationConfig: streetSweepingParams.inputValidationConfig,
                closestAddresslookupID: streetSweepingParams.parentID
              };


              COC.AddressLookup.checkFreeFormAndSelected(checkFreeFormAndSelectedParams);

              streetSweepingParams.inputValidationConfig.type = 'checkIfValidated';
              addressValidated = COC.Input.inputValidation(streetSweepingParams.inputValidationConfig);
              // did we get a theoretical back?
              theoretical = {};
              chosenReturnedData = {};
              validatedAddress = false;
              if (!
                addressValidated.validated) {
                _context.next = 13;
                break;
              }
              // set the snow validation and then get the theoretical
              validatedAddress = true;
              theoreticalParams = {
                validatedAddress: true,
                closestAddresslookupID: streetSweepingParams.closestAddresslookupID
              };

              theoretical = COC.AddressLookup.checkTheoreticalDone(theoreticalParams);
              // theoretical = await COC.AddressLookup.checkTheoreticalDone(checkTheoreticalDoneParams);
              _context.next = 18;
              break;
            case 13:
              _context.next = 15;
              return (

                streetSweepingParams.addressLookupParams.setupAddressEvents.chosenTriggered());
            case 15:
              _yield$streetSweeping = _context.sent;
              theoreticalData = _yield$streetSweeping.theoreticalData;
              // take the top match / perfect fit
              theoretical = {
                validatedAddress: validatedAddress,
                theoreticalDataValues: theoreticalData
              };
            case 18:



              getLayersParams = {
                geoAddr: theoretical.theoreticalDataValues,
                layerName: gisServerParams.mapLayerNames,
                gisServerParams: gisServerParams
              };


              getLayerData = COC.GIS.Map.getMapLayersFromTheoretical(getLayersParams);
              getLayerData.then(function(layerData) {
                processDataAndRender(layerData);
              })["catch"](function(err) {
                console.log(err);
              });
            case 21:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));
    return _getAllStreetSweepingData.apply(this, arguments);
  }


  var processDataAndRender = function processDataAndRender(dataToProcess) {
    var scheduledDate = 'ROADSNOTIFY.SCHEDULE.SCHEDULED_DT';
    var sweptDuring = 'ROADSNOTIFY.SCHEDULE.DAY_NIGHT';

    // get the address for the map link
    var address = dataToProcess.address ? dataToProcess.address : dataToProcess.address.address;
    var addressNormalized = dataToProcess.addressNormalized ? dataToProcess.addressNormalized : address;

    var nestData = dataToProcess.layers ? dataToProcess.layers : {};
    // get the attributes from each feature into an array
    var featureDetails = new Set();
    var resultsRollup = [];

    var listHtml = '';
    var appendHtml = '';
    var streetStreetingRender = function() {
      if (nestData.length >= 1) {
        // correct the data and nest the map attributes
        nestData.forEach(function(layerFeature) {
          layerFeature.forEach(function(layerFeatureItem) {
            var sweepDetails = layerFeatureItem.attributes;
            if (sweepDetails[scheduledDate]) {
              // set a human readable date using momentjs, because why would we do this manually?
              sweepDetails.correctedDate = moment(sweepDetails['ROADSNOTIFY.SCHEDULE.SCHEDULED_DT']).format('LL');
              sweepDetails.daysFromNow = moment(sweepDetails['ROADSNOTIFY.SCHEDULE.SCHEDULED_DT'], 'YYYYMMDD').fromNow();

              //future - convert to luxon, moment is depreciated
              // const { DateTime } = luxon;
              // sweepDetails.correctedDate = DateTime
              //   .fromMillis(sweepDetails[scheduledDate])
              //   .toLocaleString(DateTime.DATE_FULL);





              // you could use this if you wanted to get a human readable duration until the event
              // sweepDetails.daysFromNow = DateTime
              //   .fromMillis(sweepDetails[scheduledDate])
              //   .toRelativeCalendar();

              // set the list row html
              // sweepDetails.fromNow = sweepDetails.daysFromNow.split(' ')
              //   .map((w) => w[0].toUpperCase() + w.substr(1).toLowerCase())
              //   .join(' ');

              // set parking ban status
              sweepDetails.parkingBan = 'No';
              if (sweepDetails['RM_SCU_ROUTE.PARK_BAN_YN'] == 'Y') {
                sweepDetails.parkingBan = 'Yes';
              }

              // get the route streetName field name from the postOutFields
              var postOutFields = gisServerParams.postOutFields;
              var routeStreetName = Object.values(postOutFields.find(function(x) {
                return x.routeStreetName;
              }))[0];

              sweepDetails.html = "<li class='mb-xxs'><strong>".concat(sweepDetails.correctedDate, " start</strong> (swept during ").concat(sweepDetails[sweptDuring].toLowerCase(), ")<div>Sweeping on ").concat(sweepDetails[routeStreetName], "</div></li>");
              resultsRollup.push(sweepDetails);
            } else {
              sweepDetails.html = '<li class="mb-xxs"><strong>No schedules found for that address</strong></li>';
              resultsRollup.push(sweepDetails);
            }
          });

        });

        // sort the features
        var sortedDataFeatures = COC.Utils.sortArray(resultsRollup, scheduledDate);

        // lets use a set to de-dupe the results
        var datasetSet = new Set();

        // add only the html to the set as that is all we are outputting
        sortedDataFeatures.forEach(function(f) {
          datasetSet.add(f.html);
        });

        // iterate through the set to get the list rows
        datasetSet.forEach(function(d) {
          listHtml += d;
        });

        // format the html and append
        appendHtml = "<h4 class='results-title'>Scheduled cleaning dates in your area:</h4>\n                    <ul>".concat(
          listHtml, "</ul><a href=\"").concat(gisServerParams.mapURL).concat(address, "\" tabindex=\"0\" role=\"button\" target='_blank'>View map for ").concat(addressNormalized, "</a>");
      } else {
        // no data returned from the proxy service
        appendHtml = "<h4 class='results-title'>Sorry, we could not find street sweeping schedules for your address.</br>\n                    Please check the map for scheduled cleaning dates in your area:</h4>\n                    <ul>".concat(

          listHtml, "</ul><a href=\"https://maps.calgary.ca/StreetSweeping/?find=").concat(address, "\" tabindex=\"0\" role=\"button\" target='_blank'>View map for ").concat(addressNormalized, "</a>");
      }

      // spinner
      COC.Decorators.Spinner().hide();
      $(streetSweepingParams.addressLookupParams.resultsContainer).removeClass('d-none');
      $(streetSweepingParams.resultsWrapper).html(appendHtml);
    }();
  };

  var setUpSubmitButton = function setUpSubmitButton() {
    streetSweepingParams.submitButton.click(function(event) {
      event.preventDefault();
      COC.Decorators.Spinner().show();
      getAllStreetSweepingData();
    });
  };

  function init() {
    var closestAddresslookup = $(streetSweepingParams.parentID).closest('.address-lookup-wrapper').find('.address-lookup-container').attr('id');
    streetSweepingParams.closestAddresslookupID = "#".concat(closestAddresslookup);
    var _COC$Components$Addre =





      COC.Components.AddressLookup(streetSweepingParams.closestAddresslookupID),
      initParams = _COC$Components$Addre.initParams,
      setupAddressEvents = _COC$Components$Addre.setupAddressEvents,
      getSetFoundAddressData = _COC$Components$Addre.getSetFoundAddressData,
      checkTheoreticalDone = _COC$Components$Addre.checkTheoreticalDone;
    var
      addressLookupParams = initParams.addressLookupParams,
      inputValidationConfig = initParams.inputValidationConfig;

    streetSweepingParams.addressLookupParams = addressLookupParams;
    streetSweepingParams.inputValidationConfig = inputValidationConfig;
    streetSweepingParams.setupAddressEvents = setupAddressEvents;
    streetSweepingParams.getSetFoundAddressData = getSetFoundAddressData;
    streetSweepingParams.checkTheoreticalDone = checkTheoreticalDone;

    // move this component's results into the address results div
    $(streetSweepingParams.resultsWrapper).appendTo(addressLookupParams.resultsContainer);
    setUpSubmitButton();
  }

  init();

};
