/**
* Revenue Neutral Calculator Functions
* Files: https://spprd-authoring.calgary.ca:47443/PDA/Assessment/Scripts/Forms/AllItems.aspx?RootFolder=%2fPDA%2fAssessment%2fScripts%2frevenue_neutral_calculator&FolderCTID=0x01200034A9BCC6D622F347AB49ECA3843378A5
*
* @author Chirag, Butani , Digital Developer, chirag.butani@calgary.ca
*/


var COC = COC || {};

COC.Components.voc = (function calculator(element) {
	 	
	 	function init(){
	 		 $( window ).on( "load", function() {
	 			if ( $( "#btnOpenAssessmentChatbot,#chat-widget-container" ).length ) { 
		             $( "#pagefeedback-btn, #websitefeedback-btn" ).remove();
		             $("#pagefeedback").removeClass("delay animate__animated animate__fadeInRight animate__delay-15s");
		             $("#pagefeedback").removeClass("delay animate__animated animate__fadeInRight animate__delay-30s");
		             $("#pagefeedback").removeClass("delay animate__animated animate__fadeInRight animate__delay-45s");
		       		 $(".modal-backdrop-test").addClass("d-none");
		         }
	 	    });
	         if ( $( "#btnOpenAssessmentChatbot,#chat-widget-container" ).length ) { 
	        	 $( "#pagefeedback-btn, #websitefeedback-btn" ).remove();
	        	 $("#pagefeedback").removeClass("delay animate__animated animate__fadeInRight animate__delay-15s");
	        	 $("#pagefeedback").removeClass("delay animate__animated animate__fadeInRight animate__delay-30s");
	        	 $("#pagefeedback").removeClass("delay animate__animated animate__fadeInRight animate__delay-45s");
	       		 $(".modal-backdrop-test").addClass("d-none");
	         }
	 		var maxLength = 255;
	 		$('#description-1').keyup(function() {
	 		  var textlen = $(this).val().length ;
	 		  $('#current_comment').text(textlen).css('color', '#c8102e');
	 		});
	 		$('#description-2').keyup(function() {
		 		  var textlen = $(this).val().length ;
		 		  $('#current_comment-2').text(textlen).css('color', '#c8102e');
		 		});
	 		$('#pageFeedback-useful, #pageFeedback-description').keyup(function() {
		 		  var textlen = $(this).val().length ;
		 		  $('#page_current_comment').text(textlen).css('color', '#c8102e');
		 		});
	 		
	 	}

  return {init: init};
})();

