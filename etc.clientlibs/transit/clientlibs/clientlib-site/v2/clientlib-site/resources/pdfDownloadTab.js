
function getDropdownRouteMaps(){
    //Show links area, with loading screen
    $('#mobile-pdf-download-extender').show();
    $('#schedule-pdf-links').show();
    $('#routePdf-result-loader').show();
    $('#routePdf-result-overlay').show();
    //add delay to check if user input for widget is valid
    setTimeout(function() {
        var nextRideSearch = document.getElementById("ctSchedulesMenu").querySelectorAll('.selectmenu-text')[0].innerHTML;
        if(nextRideSearch != ""){
            setTimeout(function() {
                //FIRST check if number selected route is a route
                var nextRideNumber = nextRideSearch.match(/^\d*/);
                //check string number (first sequence)
                    //check if search field grabbed (nextRideSearch) contains the keyword from array arrayRouteMapLikeness
                    if(nextRideSearch.includes(arrayRouteMapLikeness[nextRideNumber])){
                        if(nextRideNumber < 1000){
                            //assign title variable to route name + number #routeNameTitle
                            var title = nextRideNumber +" "+ arrayRouteMap[nextRideNumber];
                            var numberFolder;
                            if(nextRideNumber < 100){
                                numberFolder = "000";
                                //make into 3 digit format (ex. 3 -> 003)
                                if(nextRideNumber < 10){
                                    nextRideNumber = "00"+nextRideNumber;
                                }else{
                                    nextRideNumber = "0"+nextRideNumber;
                                }
                            }else if(nextRideNumber < 200){
                                numberFolder = (nextRideNumber/100)+"00";
                            }
                            var queryResultRoute = getJsonFilesRoute();
                            var queryResultPocket = getJsonFilesPocket();

                            //show route number text
                            var routeDamPath = "/content/dam/transit/files/route/";
                            var pocketDamPath = "/content/dam/transit/files/pocketschedule/";

                            var routePaths = getRoutePaths(queryResultRoute,routeDamPath);
                            var pocketPaths = getRoutePaths(queryResultPocket,pocketDamPath);

                            $('#routeNameTitle').html(title);

                            //grab paths and assign to pdfMap and pocketSchedule
                            if(routePaths[nextRideNumber]){
                                $('#routePdfMapLink').attr("href",routePaths[nextRideNumber]);
                                $('#routePdfMapLink').html("Download PDF map");
                            }else{
                                $('#routePdfMapLink').removeAttr("href");
                                $('#routePdfMapLink').html("No PDF Map Available");
                            }

                            if(pocketPaths[nextRideNumber]){
                                $('#routePocketScheduleLink').attr("href",pocketPaths[nextRideNumber]);
                                $('#routePocketScheduleLink').html("Download Pocket Schedule");
                            }else{
                                $('#routePocketScheduleLink').removeAttr("href");
                                $('#routePocketScheduleLink').html("No Pocket Schedule Available");
                            }

                            //Show links in case they were hidden
                            printLinksFound();

                        }else{
                            //No links for these locations
                            printNoLinksFound();
                        }
                }else{
                    //No links for these locations
                    printNoLinksFound();
                }
                //delay 1s for better user experience
            }, 300);
        }else{
            $('#schedule-pdf-links').hide();
            $('#routePdf-result-loader').hide();
            $('#routePdf-result-overlay').hide();
        }
    },100);


}

function getJsonFilesRoute() {
    var jsonObj;
    jQuery.ajax({
        url: '/content/dam/transit/files/route.1.json',
        type: "GET",
        contentType: "text/plain",
        async: false,
        dataType: "text",
        success: function(data) {
            var text = data;
            jsonObj = JSON.parse(text);
        },
    });
    //keyValue is either route or pocketschedule
    var jsonObjKeys = Object.keys(jsonObj);
    //return value key pairs (004 -> /content/dam/.../004_apr_2019.pdf)
    return jsonObjKeys;

}
function getJsonFilesPocket() {
    var jsonObj;
    jQuery.ajax({
        url: '/content/dam/transit/files/pocketschedule.1.json',
        type: "GET",
        contentType: "text/plain",
        async: false,
        dataType: "text",
        success: function(data) {
            var text = data;
            jsonObj = JSON.parse(text);
        },
    });
    //keyValue is either route or pocketschedule
    var jsonObjKeys = Object.keys(jsonObj);
    //return value key pairs (004 -> /content/dam/.../004_apr_2019.pdf)
    return jsonObjKeys;

}

function getRoutePaths(routePathsJson, damPath){
    //currently have array with jcr:content, other, and then keys
    //ex. routePathsJson[4] == 001_apr_2019.pdf
    var urlsMapping = {};
    var keyNumber;

    for(i=0; i < routePathsJson.length; i++){
        keyNumber = routePathsJson[i].substring(0,3);
        if(keyNumber != null){

            //valid route, add to array with route identifier
            urlsMapping[keyNumber] = damPath+routePathsJson[i];

        }else{
            //misnamed assets will not be accounted for
        }

    }

    return urlsMapping;
}

function printNoLinksFound(){
    $('#routeNameTitle').html("No Available Maps Found For This Location");
    $('#routePdfMapLink').hide();
    $('#routePocketScheduleLink').hide();

    //Hide where links would be
    $('#routePdf-result-overlay').hide();
    $('#routePdf-result-loader').hide();
    return false;
}

function printLinksFound(){
    $('#routePdfMapLink').show();
    $('#routePocketScheduleLink').show();

    //Hide loading screen
    $('#routePdf-result-overlay').hide();
    $('#routePdf-result-loader').hide();
    return false;
}
/* CTM-283 Show the PDF tab if you're on the Schedules/Maps tab, otherwise hide */
function hidePDFExtension(){
    $('#mobile-pdf-download-extender').removeClass('active');
}

function showPDFExtension(){
    $('#mobile-pdf-download-extender').addClass('active');
}

