"use strict";
/*
 * @Date:   2018-12-13 13:04:13
 * @Last Modified time: 2020-05-13 17:08:38
 */

COC.Decorators = COC.Decorators || {};
COC.Decorators.Spinner = function(selector) {
  var spinnerElement = $('.content-lookup-preload-spinner');
  // if (selector) {
  //   spinnerElement = $(selector + ' .content-lookup-preload-spinner');
  // }
  var show = function show() {
    spinnerElement.removeClass('d-none');
  };

  var hide = function hide() {
    spinnerElement.addClass('d-none');
  };

  return {
    show: show,
    hide: hide
  };

};