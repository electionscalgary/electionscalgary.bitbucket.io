"use strict";
/*
 * @Date:   2018-12-13 13:04:13
 * @Last Modified time: 2021-01-25 15:03:32
 */

COC.Input = COC.Input || {};
COC.Input.inputValidation = function(inputValidationConfig) {
  var

    selectorParent =



    inputValidationConfig.selectorParent,
    selector = inputValidationConfig.selector,
    type = inputValidationConfig.type,
    message = inputValidationConfig.message;

  if (typeof selectorParent !== 'string') {
    console.warn("validatation  selectorParent must be a string");
  }
  // this should be made universal, to remove the  selectorParent type for any value
  var ariaDescribedby = selectorParent.replace('#', '');
  var validationElement = $("".concat(selectorParent, " .form-vld-msg"));

  var show = function show(messageHtml) {
    $(selector).attr('aria-describedby', ariaDescribedby);
    // remove any previous messages
    validationElement.remove();
    $(selector).after(messageHtml);
  };

  var hide = function hide() {
    // but if it has more than one aria-describedby, than we would have to add something here to deal with that
    $(selector).removeAttr('aria-describedby', '');
    validationElement.remove();
  };

  var validated = 'validation not checked';

  var messageHtml = '';
  var messageClass = '';
  var newMessage = message;
  switch (type) {
    case 'valid': {
      if (newMessage === '') {
        newMessage = 'Valid';
      }
      messageClass = 'input-validation-success-address-valid valid';
      break;
    }
    case 'invalid': {
      if (newMessage === '') {
        newMessage = 'Please enter a valid address.';
      }
      messageClass = 'input-validation-success-address-error error';
      break;
    }
    case 'warning': {
      if (newMessage === '') {
        newMessage = 'Please enter a valid address.';
      }
      messageClass = 'input-validation-success-address-warning warning';
      break;
    }
    case 'remove': {
      // silently remove the validation
      hide();
      break;
    }

    case 'checkIfValidated': {
      if (validationElement) {
        if (validationElement.hasClass('input-validation-success-address-valid')) {
          validated = true;
        } else {
          validated = false;
        }
      }
      break;
    }
    default: {}
  }


  if (type !== 'remove' && type !== 'checkIfValidated') {
    // attach newMessage
    messageHtml = "<span class=\"form-vld-msg ".concat(messageClass, "\"> ").concat(newMessage, "\n        </span>");

    show(messageHtml);
  }

  return {
    show: show,
    hide: hide,
    validated: validated
  };

};