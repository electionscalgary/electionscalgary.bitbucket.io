'use strict';

COC.GIS = COC.GIS || {};
COC.GIS.Map = COC.GIS.Map || {};

COC.GIS.Map.getMapLayersFromTheoretical = function(getMapLayersParams) {
  return new Promise(function(resolve, reject) {
    var

      geoAddr =



      getMapLayersParams.geoAddr,
      layerName = getMapLayersParams.layerName,
      gisServerParams = getMapLayersParams.gisServerParams,
      callbackFunction = getMapLayersParams.callbackFunction;

    var encodedAttributes = encodeURIComponent("".concat(geoAddr.attributes.X, ",").concat(geoAddr.attributes.Y));
    // get rings
    // default distance
    var ringsDistance = 60;
    if (gisServerParams.ringsDistance) {
      // distance override
      ringsDistance = gisServerParams.ringsDistance;
    }
    var ringsURL = "https://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer/buffer?geometries=".concat(encodedAttributes, "&inSR=3776&outSR=3776&bufferSR=3776&distances=").concat(ringsDistance, "&unit=9001&unionResults=true&geodesic=true&f=json");
    COC.GIS.GetRings(ringsURL).then(function(ringsData) {
      var rings = ringsData.geometries[0];

      // map to string the outfields
      var outFields = gisServerParams.postOutFields.map(function(postField) {
        return "".concat(Object.values(postField));
      }).join(',');
      var ringsParams = {
        geometry: JSON.stringify(rings),
        f: 'pjson',
        geometryType: 'esriGeometryPolygon',
        spatialRel: 'esriSpatialRelIntersects',
        inSR: '3776',
        outSR: '3776',
        outFields: outFields
      };


      var dataToRender = {};
      dataToRender.layers = [];
      var layerPromise = function layerPromise(layer) {
        return new Promise(function(resolve, reject) {
          // layer overrides
          var mapLayerName = "".concat(gisServerParams.mapServerURL, "/").concat(layer.id, "/query");
          if (layer.layerSuffixURL) {
            mapLayerName = "".concat(gisServerParams.mapServerURL, "/").concat(layer.layerSuffixURL, "/").concat(layer.id, "/query");
          };

          var postFieldsFound = layer.postOutFields ? layer.postOutFields.length > 0 : false;
          if (postFieldsFound) {
            ringsParams.outFields = layer.postOutFields.join(',');
          } else {
            ringsParams.outFields = gisServerParams.postOutFields.map(function(postField) {
              return "".concat(Object.values(postField));
            }).join(',');
          }

          var getLayerData = COC.GIS.GetLayerData(mapLayerName, ringsParams, layer);
          getLayerData.then(function(theLayerData) {
            resolve(theLayerData);
          })["catch"](function(err) {
            console.log(err);
            reject();
          });
        });
      };

      var getAllLayerData = function getAllLayerData(layerDetails, ringsParams) {
        return new Promise(function(resolve, reject) {
          var layerPromises = layerDetails.map(layerPromise);
          var results = Promise.all(layerPromises); // pass array of promises
          var layerDataResults = [];
          results.then(function(layerDataItems) {
            var _iterator = _createForOfIteratorHelper(
                layerDataItems),
              _step;
            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var layerDataItem = _step.value;
                if (layerDataItem.length > 0) {
                  layerDataResults.push(layerDataItem);
                }
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }
            resolve(layerDataResults);
          })["catch"](function(err) {
            console.log(err);
          });
        });
      };

      var returnedAllLayers = getAllLayerData(gisServerParams.mapLayerNames, ringsParams);
      returnedAllLayers.then(function(layerDataResults) {

        dataToRender.layers = layerDataResults;
        dataToRender.address = geoAddr.address;
        dataToRender.addressNormalized = geoAddr.addressNormalized;
        resolve(dataToRender);
      })["catch"](
        function(err) {
          console.log(err);
          reject(err);
        });
    })["catch"](
      function(err) {
        console.log(err);
      });
  });
};

// COC.EsriMap.withPinDrop = function() {
//   var validateAddressAndAddStop = function validateAddressAndAddStop(evt) {
//     return new Promise(function(resolve, reject) {
//       // intercept and validate the address on pin drop?
//       // Transform from Mercator to lat long
//       var transformToLatLong = evt.cocWebMercatorUtils.xyToLngLat(evt.mapPoint.x, evt.mapPoint.y).toString();
//       var validateAddress = COC.GIS.addressLookUp.fromLatLong(transformToLatLong);
//       validateAddress.then(function(address) {
//         // if address is invalid give a message else
//         if (address === undefined) {
//           console.log('Please enter a valid city of Calgary address');
//           reject();
//         };
//         resolve(address);
//       });
//     });
//   };
//   return {
//     validateAddressAndAddStop: validateAddressAndAddStop
//   };

// };
'use strict';
// var COC = COC || {};
// COC.GIS = COC.GIS || {};

COC.GIS.GetRings = function(ringsURL) {
  return new Promise(function(resolve, reject) {
    $.getJSON(ringsURL, function(ringsData) {
      resolve(ringsData);
    }).
    fail(function(xhr, textStatus, errorThrown) {
      reject(xhr.responseText);
    });
  });
};

COC.GIS.GetLayerData = function(mapLayerName, ringsParams, layer) {
  return new Promise(function(resolve, reject) {
    $.post(mapLayerName, ringsParams, function(postData) {
      return postData;
    }).
    done(function(returnedLayerData) {
      var parsedReturnedData = JSON.parse(returnedLayerData);
      var features;
      var foundFeatures = parsedReturnedData.features ? parsedReturnedData.features.length > 0 : false;
      if (foundFeatures) {
        features = parsedReturnedData.features;
        if (layer) {
          features.layerConfig = layer;
        }
      } else {
        features = [];
      }
      resolve(features);
    }).
    fail(function(xhr, textStatus, errorThrown) {
      reject(xhr.responseText);
    });
  });
};

COC.GIS.getAddressFromLatLong = function(latlong) {
  return new Promise(function(resolve, reject) {
    // gets an address from a lat long
    var geoCodeServer = '//gis.calgary.ca/arcgis/rest/services/pub_Locators/CalgaryAddressLocatorLL/GeocodeServer/reverseGeocode?location=';
    var gisEndPoint = window.location.protocol + geoCodeServer;
    var endPoint = "".concat(gisEndPoint).concat(latlong, "&distance=50&langCode=&outSR=&returnIntersection=false&f=json");
    var cleanUpData = function cleanUpData(data) {
      var addressCleanUp;
      if (data.address !== undefined) {
        // clean up the data
        addressCleanUp = data.address.Street;
        if (addressCleanUp.indexOf(';') > 0) {
          // take the second one, as the first is often a place name
          addressCleanUp = addressCleanUp.split(';')[1];
        }
      }
      return addressCleanUp.trim();
    };

    // get the candidates from the GIS endpoint
    $.ajax({
      url: endPoint,
      type: 'GET',
      dataType: 'json',
      success: function success(data, status) {
        var cleanedUpData = cleanUpData(data);
        resolve(cleanedUpData);
      },
      error: function error(request, errorType, errorMessage) {
        console.log('gis reverse address lookup has failed');
        reject();
      }
    });

  });
};
"use strict";
var COC = COC || {};
COC.GIS = COC.GIS || {};

// duplicate with functions in COC.GIS.Utils.js, clean up
COC.GIS.GetRings = function(ringsURL) {
  return new Promise(function(resolve, reject) {
    $.getJSON(ringsURL, function(ringsData) {
      resolve(ringsData);
    }).
    fail(function(xhr, textStatus, errorThrown) {
      reject(xhr.responseText);
    });
  });
};

COC.GIS.GetLayerData = function(mapLayerName, ringsParams) {
  return new Promise(function(resolve, reject) {
    $.post(mapLayerName, ringsParams, function(returnedLayerData) {
      var parsedReturnedData = JSON.parse(returnedLayerData);
      var features;
      if (parsedReturnedData.features) {
        features = parsedReturnedData.features;
      } else {
        features = {
          attributes: 'no features found'
        };
      }
      resolve(features);
    }).
    fail(function(xhr, textStatus, errorThrown) {
      reject(xhr.responseText);
    });
  });
};
