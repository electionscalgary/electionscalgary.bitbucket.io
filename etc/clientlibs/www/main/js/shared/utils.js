"use strict";

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _iterableToArray(iter) {
  if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) return _arrayLikeToArray(arr);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;
  for (var i = 0, arr2 = new Array(len); i < len; i++) {
    arr2[i] = arr[i];
  }
  return arr2;
} // # usage:
// let csv_data = [{dayOfWeek: 0, weather :'sunny'},{dayOfWeek: 3, weather :'foggy'},{dayOfWeek: 1, weather :'snow'}]
// let sortByWeek = COC.Util.COC.Utils.sortArray(csv_data, 'dayOfWeek');

COC.Utils = COC.Utils ? COC.Utils : {};
// sorting array items
COC.Utils.sortArray = function(array, orderField, direction) {
  var newArray = _toConsumableArray(array);
  newArray.sort(function compare(aa, bb) {
    var a;
    var b;

    if (direction) {
      if (direction === 'desc') {
        a = bb;
        b = aa;
      }
    } else {
      a = aa;
      b = bb;
    }

    // check to see if they exist
    if (typeof a[orderField] != 'undefined' && typeof b[orderField] != 'undefined') {
      if (typeof a[orderField] == 'string' && typeof b[orderField] == 'string') {
        // if they are strings make them lowercase to compare
        if (a[orderField].toLowerCase() < b[orderField].toLowerCase())
          return -1;
        else
        if (a[orderField].toLowerCase() > b[orderField].toLowerCase())
          return 1;
        else

          return 0;
      } else {
        if (a[orderField] < b[orderField])
          return -1;
        else
        if (a[orderField] > b[orderField])
          return 1;
        else

          return 0;
      }
    };
  });
  return newArray;
};
"use strict";
/*
 * @Date:   2018-12-13 13:04:13
 * @Last Modified time: 2020-08-11 10:18:03
 */

COC.Input = COC.Input || {};
COC.Input.inputValidation = function(inputValidationConfig) {
  var

    selectorParent =



    inputValidationConfig.selectorParent,
    selector = inputValidationConfig.selector,
    type = inputValidationConfig.type,
    message = inputValidationConfig.message;

  if (typeof selectorParent !== 'string') {
    console.warn("validatation  selectorParent must be a string");
  }
  // this should be made universal, to remove the  selectorParent type for any value
  var ariaDescribedby = selectorParent.replace('#', '');
  var validationElement = $("".concat(selectorParent, " .form-vld-msg"));

  var show = function show(messageHtml) {
    $(selector).attr('aria-describedby', ariaDescribedby);
    // remove any previous messages
    validationElement.remove();
    $(selector).after(messageHtml);
  };

  var hide = function hide() {
    // but if it has more than one aria-describedby, than we would have to add something here to deal with that
    $(selector).removeAttr('aria-describedby', '');
    validationElement.remove();
  };

  var validated = 'validation not checked';

  var messageHtml = '';
  var messageClass = '';
  var newMessage = message;
  switch (type) {
    case 'valid': {
      if (newMessage === '') {
        newMessage = 'Valid';
      }
      messageClass = 'input-validation-success-address-valid valid';
      break;
    }
    case 'invalid': {
      if (newMessage === '') {
        newMessage = 'Please enter a valid address.';
      }
      messageClass = 'input-validation-success-address-error error';
      break;
    }
    case 'warning': {
      if (newMessage === '') {
        newMessage = 'Please enter a valid address.';
      }
      messageClass = 'input-validation-success-address-warning warning';
      break;
    }
    case 'remove': {
      // silently remove the validation
      hide();
      break;
    }

    case 'checkIfValidated': {
      if (validationElement) {
        if (validationElement.hasClass('input-validation-success-address-valid')) {
          validated = true;
        } else {
          validated = false;
        }
      }
      break;
    }
    default: {}
  }


  if (type !== 'remove' && type !== 'checkIfValidated') {
    // attach newMessage
    messageHtml = "<span class=\"form-vld-msg ".concat(messageClass, "\"> ").concat(newMessage, "\n        </span>");

    show(messageHtml);
  }

  return {
    show: show,
    hide: hide,
    validated: validated
  };

};
