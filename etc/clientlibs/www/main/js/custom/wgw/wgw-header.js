'use strict';
var COC = COC || {};
COC.Components = COC.Components || {};

COC.Components.WGWHeader = function() {
  function setupWindowScroll() {
    $(window).on('scroll', function() {
      var timeOut = void 0;
      var checkWGWStickState = document.querySelector('.what-goes-where-header').getBoundingClientRect().top;
      if (checkWGWStickState <= 0) {

        clearTimeout(timeOut);
        timeOut = setTimeout(function() {
          $('#AtoZ h3').hide();
        }, 100);
      } else {
        clearTimeout(timeOut);
        timeOut = setTimeout(function() {
          $('#AtoZ h3').show();
        }, 100);
      }
    });
  }

  function fixSticky() {
    var stickyContentPar = $('.what-goes-where-header').height();
    $('.wgw-anchor ').css({
      position: 'relative',
      top: stickyContentPar * -1 + 'px'
    });
  }

  function init() {
    $(document).ready(function() {
      var wgwHeaderElement = $('.what-goes-where-header');
      $('.what-goes-where-header').remove();

      var aToZPage = window.location.pathname.indexOf('a-to-z-listing.html');
      var homePage = window.location.pathname.indexOf('default.html');
      var searchPage = window.location.pathname.indexOf('search.html');

      if (aToZPage > -1) {
        $('.stickycontentpar .row .col-12').append(wgwHeaderElement);
        $('.what-goes-where-header .content-block').remove();
        $('.AtoZ-link').hide();
        fixSticky();

        var resizeTimer = void 0;
        $(window).resize(function() {
          clearTimeout(resizeTimer);
          resizeTimer = setTimeout(fixSticky, 250);
        });
      } else {
        var wgwHeaderAppend = '<div class="wgwheaderpar aem-GridColumn aem-GridColumn--default--12"><div class="row no-gutters"></div>';
        $('.stickycontentpar').after(wgwHeaderAppend);
        $('.wgwheaderpar').append(wgwHeaderElement);
      }

      if (homePage == -1 && aToZPage == -1) {
        $('#wgw-AtoZ').hide();
      }

      if (searchPage > -1 || homePage > -1 || aToZPage > -1) {
        $('.what-goes-where-header .content-block-flex').remove();
      }

      // unhide as the initial state
      $('.wgw-header-container').removeClass('hide');
      $('.wgw-header-container').addClass('show');
      setupWindowScroll();
    });
  }
  init();
}();